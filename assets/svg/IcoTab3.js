import React from "react";
import {Svg, Path} from 'react-native-svg'

function IcoTab3() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <Path
        fill="#262626"
        fillRule="evenodd"
        d="M16.75.25A6.75 6.75 0 0123.5 7v10a6.75 6.75 0 01-6.75 6.75h-10A6.75 6.75 0 010 17V7A6.75 6.75 0 016.75.25h10zm0 1.5h-10A5.25 5.25 0 001.5 7v10c0 2.9 2.35 5.25 5.25 5.25h10c2.9 0 5.25-2.35 5.25-5.25V7c0-2.9-2.35-5.25-5.25-5.25zm-4.257 4.148a.75.75 0 00-.743-.648l-.102.007-.097.02A.75.75 0 0011 6v5.249l-5.25.001-.102.007A.75.75 0 005 12l.007.102a.75.75 0 00.743.648l5.25-.001V18l.007.102a.75.75 0 00.743.648l.102-.007A.75.75 0 0012.5 18v-5.25h5.25l.102-.007A.75.75 0 0018.5 12l-.007-.102a.75.75 0 00-.743-.648H12.5V6l-.007-.102z"
        clipRule="evenodd"
      ></Path>
    </Svg>
  );
}

export default IcoTab3;
