import React from "react";
import {Svg, Path} from 'react-native-svg'

function IcoHeartRed({width, height}) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/Svg"
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 38 38"
    >
      <Path
        fill="#EF5448"
        d="M20 35c-1.075-.953-2.29-1.945-3.575-3h-.017c-4.525-3.7-9.653-7.887-11.918-12.903A12.378 12.378 0 013.333 14 8.984 8.984 0 0112.5 5c1.968.003 3.893.572 5.547 1.638A9.334 9.334 0 0120 8.333a9.682 9.682 0 011.955-1.695A10.253 10.253 0 0127.5 5a8.983 8.983 0 019.167 9 12.37 12.37 0 01-1.157 5.105C33.245 24.122 28.118 28.307 23.593 32l-.016.013c-1.287 1.049-2.5 2.04-3.575 3L20 35z"
      ></Path>
    </Svg>
  );
}

export default IcoHeartRed;
