import React from "react";
import {Svg, Path} from 'react-native-svg'

function IcoTab2() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/Svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <Path fill="#FAFAFA" fillOpacity="0.01" d="M0 0H24V24H0z"></Path>
      <Path
        fill="#262626"
        fillRule="evenodd"
        d="M19.75 10c0-5.385-4.365-9.75-9.75-9.75S.25 4.615.25 10s4.365 9.75 9.75 9.75a9.712 9.712 0 006.344-2.346l5.99 5.99.084.073a.75.75 0 00.976-1.133l-5.99-5.99A9.712 9.712 0 0019.75 10zm-18 0a8.25 8.25 0 1116.5 0 8.25 8.25 0 01-16.5 0z"
        clipRule="evenodd"
      ></Path>
    </Svg>
  );
}

export default IcoTab2;
