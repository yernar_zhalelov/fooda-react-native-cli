import React from "react";
import {Svg, Path} from 'react-native-svg'

function SplashScreen() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="143"
      height="214"
      fill="none"
      viewBox="0 0 143 214"
    >
      <Path
        fill="#fff"
        d="M134.439 129.039h-82.91V214H.113V.719H142.79v39.697h-91.26v49.072h82.91v39.551z"
        opacity="0.3"
      ></Path>
    </Svg>
  );
}

export default SplashScreen;