import React from "react";
import {Svg, Path} from 'react-native-svg'

function IcoTab1() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <Path fill="#FAFAFA" fillOpacity="0.01" d="M0 0H24V24H0z"></Path>
      <Path
        fill="#262626"
        fillRule="evenodd"
        d="M12.7.904l10 9.802a1 1 0 01.3.714V23.5a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5V18a3 3 0 00-2.824-2.995L12 15a3 3 0 00-2.995 2.824L9 18v5.5a.5.5 0 01-.5.5h-7a.5.5 0 01-.492-.41L1 23.5V11.42a1 1 0 01.3-.714l10-9.802a1 1 0 011.4 0z"
        clipRule="evenodd"
      ></Path>
    </Svg>
  );
}

export default IcoTab1;
