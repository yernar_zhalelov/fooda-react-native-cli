import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'
import { color } from 'react-native-reanimated'

export const FormButtonSmallHalf = ({buttonTitle, onPress, isRed, ...rest}) => {
    return (
        <TouchableOpacity style={isRed?styles.buttonContainerRed:styles.buttonContainerGreen} onPress={onPress} { ...rest }>
            <Text style={styles.buttonText}>{buttonTitle}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainerRed: {
        marginTop: 10,
        alignSelf: 'center',
        //marginLeft: (screenWidth - 184) * 0.5, 
        width: 92, 
        height: 47,
        backgroundColor: '#EF5448',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
     },
     buttonContainerGreen: {
        marginTop: 10,
        alignSelf: 'center',
        //marginLeft: (screenWidth - 184) * 0.5, 
        width: 92, 
        height: 47,
        backgroundColor: '#85B057',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
     },
     buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
        fontFamily: 'roboto-regular',
     },
})