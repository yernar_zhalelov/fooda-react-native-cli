import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

export const FormButton = ({buttonTitle, onPress, ...rest}) => {
    return (
        <TouchableOpacity style={styles.buttonContainer} onPress={onPress} { ...rest }>
            <Text style={styles.buttonText}>{buttonTitle}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 10,
        //marginLeft: (screenWidth - 251) * 0.5,
        alignSelf: 'center',
        width: 251, 
        height: 63,
        backgroundColor: '#85B057',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
     },
     buttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff',
        fontFamily: 'roboto-regular',
     },
})