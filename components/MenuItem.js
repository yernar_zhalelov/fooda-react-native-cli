import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, ImageBackground } from 'react-native'

import { url } from '../screens/AppConfig'

const width = Dimensions.get('screen').width
const height = Dimensions.get("screen").height

const gorchica = require('../assets/images/groupitems/gorchica.jpg')
const beef = require('../assets/images/groupitems/beef.jpg')
const beef_fillet = require('../assets/images/groupitems/beef_fillet.jpg')

const numColumns2 = 2;

export const MenuItem = ({ navigation, item, index, identifier, }) => {

    const handleClick = () => {

        requestAnimationFrame(() => {

            if(item.childs == undefined) {
                navigation.navigate('SubSubCategory', {item: item, title: item.name})
            } else {
                if(identifier == 'Menu') {
                    navigation.navigate('Category', {category: item.childs, title: item.name})
                } else if(identifier == 'Category') {
                    navigation.push('SubCategory', {category: item.childs, title: item.name})
                } else if(identifier == 'SubCategory') {
                    navigation.push('Category', {category: item.childs, title: item.name})
                }
            }
        })

    }

    const renderImage = () => {
        if(item.imgProduct) {
            return {uri: url + '/imgProduct/' + item.imgProduct}
        } 

        if(item.img_name) {
            return {uri: item.img_name}
        }
    }

    const renderName = () => {
        if(item.name) {
            return item.name
        }

        if(item.nameProduct) {
            return item.nameProduct
        }
    }

    return(
        <TouchableOpacity style={styles.item} onPress={() => handleClick()}>
            {/* <View style={styles.item2}> */}
                <View style={{
                    width: '100%', 
                    resizeMode: 'contain', 
                    alignItems: 'center', 
                    justifyContent: 'center', 
                    margin: 0, 
                    }}>
                    <Image source={renderImage()} style={styles.image} />
                </View>       
            {/* </View> */}
            <Text style={styles.itemTitle}>{renderName()}</Text>
        </TouchableOpacity>   
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#fff',
        height: Dimensions.get('window').width * 0.6, // approximate a square 
        flex: 1/2, 
    },
    item2: {        
        marginTop: -15,
        borderWidth: 0,
        borderColor: '#000',
        width: '100%', 
        height: '95%', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    item3: {
        width: '100%', 
        resizeMode: 'contain', 
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    image: {
        height: '90%',
        width: '90%',
        resizeMode: 'cover',
        borderRadius: 10, 
        backgroundColor: 'grey'
    },
    shadow: {
        width: '100%', 
        resizeMode: 'contain',
    },
    itemTitle: {
        width: '95%',
        marginTop: -5,
        fontFamily: 'roboto-regular', 
        fontSize: 13,
        fontWeight: '700',
        alignSelf: 'center',
        textAlign: 'center',
        color: '#000',
    },  
})