import React from 'react'
import { View, TextInput, StyleSheet, Image, Platform } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

export const SearchInput = ({labelValue, placeholderText, setText, ...rest}) => {
    return (
        <View style={styles.inputContainer}>
            <View style={styles.iconWrapper}>
                <Image source={require('../assets/images/icoSearch.png')} style={styles.icon} />
            </View>
            <TextInput 
                value={labelValue}
                style={styles.input}
                numberOfLines={1}
                placeholder={placeholderText}
                placeholderTextColor='#5E5E5E'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                spellCheck={false}
                keyboardType={Platform.OS === 'android' ? 'visible-password' : 'default'}
                {...rest}
                onChangeText={(text) => setText(text)}
            />            
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 5,
        marginBottom: 10,
        marginLeft: screenWidth * 0.06,  
        width: screenWidth * 0.88,   
        height: screenHeight * 0.06,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
    },
    iconWrapper: {
        padding: 10,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
    },
    input: {
        flex: 1,
        fontSize: 14,        
        fontFamily: 'roboto-regular',
        color: '#303030',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        position: 'absolute',
        left: 15,
    },
})