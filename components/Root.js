import React, {useState} from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Animated, KeyboardAvoidingView } from 'react-native'
import { Menu } from '../components/Menu'

export const Root = ({buttonTitle, onPress, children, ...rest}) => {
    const [isHidden, setIsHidden] = useState(true)
    const [bottomPos, setBottomPos] = useState(new Animated.Value(0));
    const [clickPanelHeight, setClickPanelHeight] = useState('7%')
  
    {/*const rotateSpring = () => {   
   
        if(isHidden)
        {
            setBottomPos(new Animated.Value(0))
            Animated.timing(bottomPos, {
                toValue: 100,
                duration: 500,
                useNativeDriver: true,
            }).start();    
            setClickPanelHeight('15%')         
        }
        else
        {
            setClickPanelHeight('7%')
        }
        setIsHidden(!isHidden)      
    };*/}
  
    return (
        <View style={styles.container}>
            <View style={styles.container}>
             {children}
            </View>
            <View style={{position: 'absolute', bottom: 0}}><Menu /></View>
            {/*<TouchableOpacity activeOpacity={1} style={{position: 'absolute', bottom: 0, width: '100%', height: clickPanelHeight}} onPress={()=>rotateSpring()}>
                
            </TouchableOpacity>*/}          
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
    },
})