import React from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, PixelRatio } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'
import ArrowBack from '../assets/svg/ArrowBack'

export const NavigationBar = ({onPress, screenTitle, identificator, openModal, }) => {
    function renderModalButton() {
        if(identificator == true) {
            return (
                <TouchableOpacity
                    onPress={openModal}
                    style={{
                        width: 50, 
                        height: 50,
                        alignItems: 'center', 
                        justifyContent: 'center', 
                    }}
                >

                    <Image source={require('../assets/images/icoQuestion2.png')} style={{width: 25, height: 25, tintColor: '#85b057', }} />
                
                    
                </TouchableOpacity>
            )
        } else {
            return (
                <View style={styles.emptyView}>
                    <Text> </Text>
                </View>
            )
        }
    }


    return (
        <View style={styles.navbar}>
             <TouchableOpacity 
                style={styles.navbutton}
                onPress={onPress}>
                {/* <Image style={{alignSelf: 'center'}}  source={require('../assets/images/icoBackArrow.png')} />             */}
                <View style={{
                    alignSelf: 'center', 
                    width: 15,
                    height: 15,
                }}>
                    <ArrowBack/>
                </View>
                
            </TouchableOpacity>  

            <View style={styles.navtextView}>
                <Text 
                    style={styles.navtext}
                    adjustsFontSizeToFit={true}
                    allowFontScaling={true}
                    numberOfLines={10}
                >{screenTitle}</Text>
            </View>

           

            {renderModalButton()}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'green',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    navbar: {
        height: screenHeight * 0.1,
        justifyContent: 'space-between',
        flexDirection: 'row', 
        alignItems: 'center', 
    },
    navtext: {
        fontFamily: 'roboto-regular', 
        fontWeight: 'bold', 
        color: '#303030',  
        alignSelf: 'center',
        fontSize: 20
    },
    navtextView: {
        flex: 6, 
        textAlign: 'center',
        alignSelf: 'center',
        // backgroundColor: 'green', 
        height: '90%', 
        justifyContent: 'center'
    },
    navbutton: {
        flex: 1,
        height: 40, 
        justifyContent: 'center',
        // backgroundColor: 'blue'
    },
    emptyView: {
        flex: 1, 
        // backgroundColor: 'yellow'
    }
})