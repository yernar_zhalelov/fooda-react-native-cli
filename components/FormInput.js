import React from 'react'
import { View, TextInput, StyleSheet, Image, Platform } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'

export const FormInput = ({labelValue, placeholderText, iconType, changeText, ...rest}) => {
    const getImage = name => { 
        switch (name) { 
            case "mail": return require('../assets/images/icoEmail2.png')
            case "password": return require('../assets/images/icoPassword2.png')
            case "person": return require('../assets/images/icoPerson.png')
            case "phone": return require('../assets/images/icoPhone.png')
            default: return null
        } 
    }

    if(iconType.localeCompare('mail')) {
        return (
            <View style={styles.inputContainer}>
                <TextInput 
                    value={labelValue}
                    style={styles.input}
                    numberOfLines={1}
                    placeholder={placeholderText}
                    placeholderTextColor='#5E5E5E'
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    {...rest}
                    secureTextEntry
                    password={true}
                    onChangeText={(text) => changeText(text)}
                />
                <View style={styles.iconWrapper}>
                    <Image source={getImage(iconType)} style={styles.icon} />
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.inputContainer}>
                <TextInput 
                    value={labelValue}
                    style={styles.input}
                    numberOfLines={1}
                    placeholder={placeholderText}
                    placeholderTextColor='#5E5E5E'
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    {...rest}
                    keyboardType={Platform.OS === 'android' ? 'visible-password' : 'default'}
                    onChangeText={(text) => changeText(text)}
                />
                <View style={styles.iconWrapper}>
                    <Image source={getImage(iconType)} style={styles.icon} />
                </View>
            </View>
        )
    }

    
}

const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 5,
        marginBottom: 10,
        alignSelf: 'center',
        width: screenWidth * 0.7,   
        height: screenHeight * 0.05,
        borderColor: '#ACABAB',
        borderRadius: 10,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    iconWrapper: {
        padding: 10,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
    },
    input: {
        padding: 10,
        marginLeft: 5,
        flex: 1,
        fontSize: 14,        
        fontFamily: 'roboto-regular',
        color: '#303030',
        justifyContent: 'center',
        alignItems: 'center',
        ...Platform.select({
            ios: {
                height: 50
            }
        })
    },
    icon: {
        position: 'absolute',
        right: 15,
        tintColor: '#8cbd65',
        height: screenHeight * 0.03, 
        width: screenWidth * 0.06
    },
})