import React, { useState } from 'react'
import { View, Text, StyleSheet, Button, Modal, TouchableOpacity, Image, ImageBackground } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'
import { useNavigation } from '@react-navigation/native';

import IcoTab1 from '../assets/svg/IcoTab1';
import IcoTab2 from '../assets/svg/IcoTab2';
import IcoTab3 from '../assets/svg/IcoTab3';
import IcoTab4 from '../assets/svg/IcoTab4';
import IcoTab5 from '../assets/svg/IcoTab5';

export const Menu = ({}) => {
    const navigation = useNavigation();
    
    const goMenu = () => {
        navigation.navigate('Menu')
    }

    const goSearch = () => {
        navigation.navigate('Search')
    }
    const goAddProduct = () => {
        navigation.navigate('AddProduct')
    }
    const goFavorites = () => {
        navigation.navigate('Favorites')
    }
    const goAccount = () => {
        navigation.navigate('Account')
    }
    
    return (
            <View style={{width: '100%', height: 45, backgroundColor: '#fff', borderTopLeftRadius: 10, borderTopRightRadius: 10, flexDirection: 'row' }}>
                <View style={{width: screenWidth/5, height: 60, justifyContent: 'center', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.button} onPress={() => goMenu()}>
                        <View>
                            <IcoTab1 />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width: screenWidth/5, height: 60, justifyContent: 'center', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.button} onPress={() => goSearch()}>
                        <View>
                        <IcoTab2 />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width: screenWidth/5, height: 60, justifyContent: 'center', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.button} onPress={() => goAddProduct()}>
                        <View>
                        <IcoTab3/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width: screenWidth/5, height: 60, justifyContent: 'center', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.button} onPress={() => goFavorites()}>
                        <View>
                        <IcoTab4/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width: screenWidth/5, height: 60, justifyContent: 'center', alignSelf: 'center' }}>
                    <TouchableOpacity style={styles.button} onPress={() => goAccount()}>
                        <View>
                        <IcoTab5/>
                        </View>
                    </TouchableOpacity>
                </View>                
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#85B057',
        width: '100%',
        height: '100%',
    },
    container2: {
        backgroundColor: '#85B057',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainbutton: {
        width: '100%', 
        height: '100%',
    },
    button: {
        width: screenWidth/5, 
        height: 50, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    fImage: {
        height: 352, 
        width: 165,
    },
    fTextHolder: {
        flex:1,
        alignItems:'center',
        justifyContent: 'center', 
    },
    fText: {
        fontFamily: "roboto-bold",
        fontSize: 36,
        color: 'white',
        fontWeight: 'bold',
    },
})