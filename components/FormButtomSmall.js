import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

export const FormButtonSmall = ({buttonTitle, onPress, ...rest}) => {
    return (
        <TouchableOpacity style={styles.buttonContainer} onPress={onPress} { ...rest }>
            <Text style={styles.buttonText}>{buttonTitle}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 10,
        alignSelf: 'center',
        //marginLeft: (screenWidth - 184) * 0.5, 
        width: 184, 
        height: 47,
        backgroundColor: '#85B057',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
     },
     buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
        fontFamily: 'roboto-regular',
     },
})