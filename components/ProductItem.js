import React, {useState, useEffect} from 'react'
import { View, TextInput, StyleSheet, Image, Text, Dimensions, TouchableOpacity } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import {url} from '../screens/AppConfig'
import AsyncStorage from '@react-native-async-storage/async-storage';

import IcoHeart from '../assets/svg/IcoHeart';
import IcoHeartRed from '../assets/svg/IcoHeartRed';

const like = require('../assets/images/svg/icoLike.svg')
const dislike = require('../assets/images/svg/icoDislike.svg')
const nolike = require('../assets/images/svg/icoNolike.svg')

const numColumns = 2;

const height = Dimensions.get('screen').height

const getImage = (status) => {
    if (status == "Не рекомендую") {
        return dislike
    } else if (status == "Не могу рекомендовать") {
        return nolike
    } else if (status == "Рекомендую") {
        return like
    }
}

const renderImage = (img) => {
    if(img.includes('http')) {
        return img
    } else {
        return url + '/imgProduct/' + img
    }
}

export const ProductItem = ({navigation, item, index}) => {
    const [status, setStatus] = useState(false)
    const [product, setProduct] = useState(undefined)

    useEffect(() => {
        async function getData() {
            let fav = await AsyncStorage.getItem('favourites')
            let all = await AsyncStorage.getItem('allProducts')

            if(JSON.parse(fav).find(e => e.nameProduct === item.nameProduct) != undefined) {
                setStatus(true)
            } else {
                setStatus(false)
            }

            setProduct(JSON.parse(all).find(e => e.nameProduct === item.nameProduct)) 
        }

        getData()
        
    },[])


    const renderHeartIcon = () => {
        
        if(status == true) {
            return (
                <View style={{position: 'absolute', right: 5, top: 4}}> 
                    <IcoHeartRed
                        width='22'
                        height='20'
                    />
                </View>
            )
        }

        if(status == false) {
            return (
                <View style={{position: 'absolute', right: 5, top: 4}}> 
                    <IcoHeart
                        width='22'
                        height='20'
                    />
                </View>
            )
        }
    }

    const renderIcon = () => {

        let result = []
        let i = 5

        if(product != undefined) {
            if(product.icon != undefined) {
                product.icon.forEach(element => {
                    result.push(
                        <Image 
                            source={{uri: element.icon}}
                            style={{
                                position: 'absolute', 
                                left: 10, 
                                top: i, 
                                width: 22, 
                                height: 20,
                                resizeMode: 'contain'
                            }}
                        />
                    )

                    i = i + (Dimensions.get('screen').width / numColumns) * 0.3
                });
            } 
        }
        
        return result

    }

    const renderVivodIcon = () => {
        if(product != undefined) {
            
            return (
                <Image 
                    source={{uri: product.vivod_icon}}
                    style={{
                        position: 'absolute', 
                        right: 6, 
                        top: Dimensions.get('screen').width / numColumns - 73, 
                        width: 32, 
                        height: 32, 
                        resizeMode: 'contain'
                    }}
                />
            )
        } else {
            return null
        }
    }

    if (item.empty === true) {
        return <View style={[styles.item, styles.itemInvisible]} />;
      }
      return (
          <TouchableOpacity activeOpacity={1} style={styles.item} onPress={() => navigation.navigate('ProductDetails', {product: item})}>
              <View style={styles.item2}>
                  <Image source={{uri: renderImage(item.imgProduct)}} style={styles.image} />
                  {renderIcon()}
                  {renderHeartIcon()}
                  {renderVivodIcon()}
              </View>              
              <Text style={styles.itemTitle}>{item.nameProduct}</Text> 
          </TouchableOpacity>        
      );
}

const styles = StyleSheet.create({
    item: {
        width: '100%', 
        height: '100%',
        backgroundColor: '#fff',
        flex: 1,
        marginHorizontal: 5,
        marginVertical: 5,
        maxWidth: (Dimensions.get('screen').width - 40) / numColumns,
        height: Dimensions.get('screen').width / numColumns, // approximate a square, 
    },
    item2: {
        borderWidth: 0,
        width: '100%', 
        height: '80%', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    image: {
        height: '70%',
        resizeMode: 'contain',
        width: '70%', 
    },
    itemTitle: {
        marginLeft: 3,
        fontFamily: 'roboto-regular', 
        fontSize: 10, 
        color: '#000',
    },    
})