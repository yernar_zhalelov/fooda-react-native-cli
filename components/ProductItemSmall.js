import React, {useEffect, useState} from 'react'
import { View, TextInput, StyleSheet, Image, Text, Dimensions, TouchableOpacity } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import AsyncStorage from '@react-native-async-storage/async-storage';
import ImageColors from 'react-native-image-colors'
import { AutoSizeText, ResizeTextMode } from 'react-native-auto-size-text';

import IcoHeartRed from '../assets/svg/IcoHeartRed';
import IcoHeart from '../assets/svg/IcoHeart';

const like = require('../assets/images/svg/icoLike.svg')
const dislike = require('../assets/images/svg/icoDislike.svg')
const nolike = require('../assets/images/svg/icoNolike.svg')

const numColumns = 3;

let rawIndex = 0


export const ProductItemSmall = ({navigation, item, index, }) => {
    const [status, setStatus] = useState(false)
    const [product, setProduct] = useState(item)

    async function collectFav () {
        let fav = await AsyncStorage.getItem('favourites')
        

        if(JSON.parse(fav).find(e => e.nameProduct === item.nameProduct) != undefined) {
            setStatus(true)
        } else {
            setStatus(false)
        }
 
        
    }

    const getColor = () => {
        if(product != undefined) {
            if(product.vivod_color != undefined) {
                if(product.vivod_color == null) {
                    return 'black'
                } else if(product.vivod_color == 'yellow') {
                    return '#ffeb81'
                } else if(product.vivod_color == 'red') {
                    return '#ff7060'
                } else if(product.vivod_color == 'green') {
                    return '#89b927'
                }
            } 
        }
    } 

    useEffect(() => {
        collectFav()

        navigation.addListener('focus', () => {
            collectFav()
        })

        return () => {
            navigation.removeListener()
        }
    },[navigation])


    const renderHeartIcon = () => {
        
        if(status == true) {
            return (
                <View style={{position: 'absolute', right: 5, top: 4 }}> 
                    <IcoHeartRed
                        width='27'
                        height='25'
                    />
                </View>
            )
        }

        if(status == false) {
            return (
                <View style={{position: 'absolute', right: 5, top: 4 }}> 
                    <IcoHeart
                        width='27'
                        height='25'
                    />
                </View>
            )
        }
    }

    const renderIcon = () => {
        let result = []
        let i = 1

        if(product != undefined) {
            if(product.icon != undefined) {
                product.icon.forEach(element => {
                    result.push(
                        <Image 
                            source={{uri: element.icon}}
                            style={{
                                position: 'absolute', 
                                left: 5, 
                                top: i + 5, 
                                width: 22, 
                                height: 20,
                                resizeMode: 'contain'
                            }}
                        />
                    )

                    i = i + (Dimensions.get('screen').width / 2.5) * 0.26
                });
            } 
        }
        
        return result
    }

    const renderVivodIcon = () => {
        if(product != undefined) {
            return (
                <Image 
                    source={{uri: product.vivod_icon}}
                    style={{
                        position: 'absolute', 
                        right: 5, 
                        top: Dimensions.get('screen').width / 2.5 * 0.58, 
                        width: 20, 
                        height: 20, 
                        resizeMode: 'contain'
                    }}
                />
            )
        } else {
            return null
        }
    }

    
    return (
        <TouchableOpacity activeOpacity={1} style={styles.item} onPress={() => navigation.navigate('ProductDetails', {product: item})}>

            <View style={[styles.item2, {borderColor: getColor()}]}>
                <Image source={{uri: item.imgProduct}} style={styles.image} />
                {renderIcon()}
                {renderHeartIcon()}
                {renderVivodIcon()}

            </View>


            

            {/* <Text 
                style={styles.itemTitle}
                allowFontScaling={true}
                adjustsFontSizeToFit={true}
            >
                {item.nameProduct}
            </Text> */}

            <View style={{
                marginTop: 8,
                marginLeft: 3,
            }}>

                <AutoSizeText 
                    style={styles.itemTitle}
                    mode={ResizeTextMode.max_lines}
                    numberOfLines={4}
                    fontSize={10}
                >
                    {item.nameProduct}
                </AutoSizeText>

            </View>

            

        </TouchableOpacity>        
    );
        
    
    
}

const styles = StyleSheet.create({
    item: {
        // flex: 1,
        marginHorizontal: 6,
        width: Dimensions.get('screen').width / 3.41,
        // height: Dimensions.get('screen').width / 2.5, // approximate a square
    },
    item2: {
        marginTop: 5, 
        borderRadius: 12, 
        backgroundColor: '#fff',  
        width: Dimensions.get('screen').width / 3.41, 
        height: Dimensions.get('screen').width * 0.32, 
        justifyContent: 'center', 
        alignItems: 'center', 
        alignSelf: 'center', 
        borderWidth: 2, 
        
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    image: {
        height: '60%',
        resizeMode: 'contain',
        width: '60%',
    },
    itemTitle: {
        fontFamily: 'roboto-regular',  
        color: '#000',
    },    
})