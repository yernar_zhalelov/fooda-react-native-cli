import React from 'react'
import { Text, TouchableOpacity, StyleSheet, ImageBackground, View } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

export const SubscribeButton = ({price, monthText, onPress, ...rest}) => {
    return (
        <TouchableOpacity style={{width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 5 }} onPress={onPress}>
            <ImageBackground source={require('../assets/images/subscribebg.png')} style={{width: 320, height: 94 }}>
            <ImageBackground source={require('../assets/images/subscribe.png')} style={{width: 209, height: 80, position: 'absolute', right: 0 }}>
                
            </ImageBackground>
            <Text style={styles.textprice}>{price}</Text>
            <Text style={styles.textwhite1}>ОФОРМИТЬ</Text>
            <View style={styles.textwhite2holder}>
                    <Text style={styles.textwhite2}>{monthText}</Text>
                </View>
            </ImageBackground>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    textprice: {
        marginTop: 10,
        marginLeft: 25,        
        fontFamily: 'roboto-bold', 
        fontSize: 24, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
        position: 'absolute', left: 10, top: 12
    },
    textwhite1: {
        marginTop: 10,
        marginLeft: 25,
        fontFamily: 'roboto-bold', 
        fontSize: 20, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#fff',
        marginBottom: 5,
        position: 'absolute', 
        right: 20, 
        top: 5,
    },
    textwhite2holder: {
        width: 120,
        marginTop: 10,
        marginLeft: 25,
        marginBottom: 5,
        position: 'absolute',
        top: 40,
        right: 15,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    textwhite2: {
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#fff',
        marginBottom: 5,
        position: 'absolute',
    },
})