import React from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'


export const SubscriptionOffModal = ({visible, onClose}) => {
    return (
        <Modal visible={visible}
            animationType={'slide'}
            backdropColor='gray'
            transparent={true}
            visible={visible}
            onBackdropPress={()=>setVisibility(false)}>
            <View style={styles.modalBackground}>
                <View style={{width: screenWidth * 0.85, height: screenWidth * 0.6, backgroundColor: '#fff', borderRadius: 20, borderColor: '#85B057', borderWidth: 1}}>
                    <TouchableOpacity 
                        style={{position: 'absolute', right: 10, top: 10}}
                        onPress={onClose}>
                        <Image source={require('../assets/images/icoClose.png')} style={{width: 28, height: 28}} />
                    </TouchableOpacity>
                    <Image style={{alignSelf:'center', marginTop: 30}} source={require('../assets/images/icoCheck.png')} />
                    <Text style={styles.navButtonText2}>Подписка отменена!</Text>
                    <Text style={styles.textdark}>С вашей карты не будут списаны средства</Text>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
    },
    navButtonText2: {         
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
        alignSelf: 'center',
        marginTop: 25,
    },
    textdark: {
        marginTop: 10,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 5,
        alignSelf:'center',
        width: '80%',
    },
})