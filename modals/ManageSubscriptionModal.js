import React from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

import { FormButtonMedium } from '../components/FormButtonMedium'


export const ManageSubscriptionModal = ({visible, onClose, onSubmit}) => {
    return (
        <Modal visible={visible}
            animationType={'slide'}
            backdropColor='gray'
            transparent={true}
            visible={visible}
            onBackdropPress={()=>setVisibility(false)}>
            <View style={styles.modalBackground}>
                <View style={{width: screenWidth * 0.85, height: screenWidth * 0.9, backgroundColor: '#fff', borderRadius: 20, borderColor: '#85B057', borderWidth: 1}}>
                    <TouchableOpacity 
                        style={{position: 'absolute', right: 10, top: 10}}
                        onPress={onClose}>
                        <Image source={require('../assets/images/icoClose.png')} style={{width: 28, height: 28}} />
                    </TouchableOpacity>
                    <Text style={styles.navButtonText2}>Управление подпиской</Text>
                    <Text style={styles.textdark}>Ваша подписка действительна до 30.01.2021</Text>
                    <Text style={styles.textdark2}>Подписка будет возобновляться автоматически,</Text>
                    <Text style={styles.textdark2}>пока вы ее не отмените. Подписку можно</Text>
                    <Text style={styles.textdark2}>возобновить в любой момент в личном</Text>
                    <Text style={styles.textdark2}>кабинете приложения</Text>
                    <View style={{marginTop: 5}}>
                        <TouchableOpacity 
                            style={{width: '100%', alignItems: 'center', marginTop: 20, marginBottom: 15}}
                            onPress={onSubmit}>
                            <Text style={styles.navButtonText}>Подробнее</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
    },
    navButtonText2: {         
        fontSize: 22, 
        fontWeight: 'bold', 
        color: '#303030',
        textAlign: 'center', 
        fontFamily: 'roboto-regular',
        alignSelf: 'center',
        marginTop: 25,
        width: '50%'
    },
    textdark: {
        marginTop: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 15,
        alignSelf:'center',
        width: '80%',
    },
    textdark2: {
        marginTop: 0,
        fontFamily: 'roboto-regular', 
        fontSize: 13, 
        fontWeight: 'normal', 
        textAlign: 'center', 
        alignSelf: 'center', 
        color: '#303030',
        marginBottom: 0,
        width: '100%',
    },
    navButtonText: {         
        fontSize: 20, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
        textDecorationLine: 'underline',
    },
})