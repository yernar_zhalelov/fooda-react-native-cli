import React from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'

import { FormInput } from '../components/FormInput'
import { FormButtonMedium } from '../components/FormButtonMedium'


export const ChangePasswordModal = ({visible, onClose, onForgot, onSubmit, oldPassword, newPassword, newPasswordConfirmation, setOld, setNew, setConfirmationNew}) => {
    return (
        <Modal visible={visible}
            animationType={'slide'}
            backdropColor='gray'
            transparent={true}
            visible={visible}
            onBackdropPress={()=>setVisibility(false)}>
            <View style={styles.modalBackground}>
                <View style={{width: screenWidth * 0.85, height: screenWidth * 1.12, backgroundColor: '#fff', borderRadius: 20, borderColor: '#85B057', borderWidth: 1}}>
                    <TouchableOpacity 
                        style={{position: 'absolute', right: 10, top: 10}}
                        onPress={onClose}>
                        <Image source={require('../assets/images/icoClose.png')} style={{width: 28, height: 28}} />
                    </TouchableOpacity>
                    <Text style={styles.navButtonText2}>Изменить пароль</Text>
                    <View style={{marginTop: 20}}>
                        <FormInput
                            placeholderText='Старый пароль*' 
                            iconType=''
                            labelValue={oldPassword}
                            changeText={(text) => setOld(text)}
                        />
                    </View>
                    <View style={{marginTop: 10}}>
                        <FormInput
                            placeholderText='Новый пароль*' 
                            iconType=''
                            labelValue={newPassword}
                            changeText={(text) => setNew(text)}
                        />
                    </View>
                    <View style={{marginTop: 10}}>
                        <FormInput
                            placeholderText='Повторите новый пароль*' 
                            iconType=''
                            labelValue={newPasswordConfirmation}
                            changeText={(text) => setConfirmationNew(text)}
                        />
                    </View>
                    <TouchableOpacity 
                        style={{width: '100%', alignItems: 'center', marginTop: 15, marginBottom: 25}}>
                        <Text style={styles.textPrivate} onPress={onForgot}>Забыли пароль?</Text>
                    </TouchableOpacity>
                    <View style={{marginTop: 0}}>
                        <FormButtonMedium
                            onPress={onSubmit}
                            buttonTitle='Сохранить'
                        />
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
    },
    navButtonText2: {         
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
        alignSelf: 'center',
        marginTop: 25,
    },
    textPrivate: {
        fontFamily: 'roboto-regular',
        flexDirection: 'row', 
        flexWrap: 'wrap',
        justifyContent: 'center',
        color: '#ACABAB',
        fontSize: 14, 
    },
})