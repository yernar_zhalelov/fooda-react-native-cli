import React from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import { bootstrap } from '../utils/bootstrap'

import { FormButtonSmallHalf } from '../components/FormButtonSmallHalf'


export const SubscriptionOffConfirmModal = ({visible, onClose, onSubmit, onDiscard}) => {
    return (
        <Modal visible={visible}
            animationType={'slide'}
            backdropColor='gray'
            transparent={true}
            visible={visible}
            onBackdropPress={()=>setVisibility(false)}>
            <View style={styles.modalBackground}>
                <View style={{width: screenWidth * 0.85, height: screenWidth * 0.9, backgroundColor: '#fff', borderRadius: 20, borderColor: '#85B057', borderWidth: 1}}>
                    <TouchableOpacity 
                        style={{position: 'absolute', right: 10, top: 10}}
                        onPress={onClose}>
                        <Image source={require('../assets/images/icoClose.png')} style={{width: 28, height: 28}} />
                    </TouchableOpacity>
                    <Text style={styles.navButtonText2}>Управление подпиской</Text>
                    <Text style={styles.textdark}>Вы уверены, что хотите отменить подписку?</Text>                    
                    <View style={{marginTop: 5, marginBottom: 30, flexDirection: 'row',  justifyContent: 'center', alignItems: 'center'}}>
                        <FormButtonSmallHalf
                            onPress={onSubmit}
                            buttonTitle='Да'
                            isRed={false}
                        />
                        <View style={{width: 20}}></View>
                        <FormButtonSmallHalf
                            onPress={onDiscard}
                            buttonTitle='Нет'
                            isRed={true}
                        />
                    </View>
                    <Text style={styles.textdark2}>Подписку можно возобновить в любой</Text>
                    <Text style={styles.textdark2}>момент в личном кабинете приложения</Text>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
    },
    navButtonText2: {         
        fontSize: 22, 
        fontWeight: 'bold', 
        color: '#303030',
        textAlign: 'center', 
        fontFamily: 'roboto-regular',
        alignSelf: 'center',
        marginTop: 25,
        width: '50%'
    },
    textdark: {
        marginTop: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 15,
        alignSelf:'center',
        width: '80%',
    },
    textdark2: {
        marginTop: 0,
        fontFamily: 'roboto-regular', 
        fontSize: 12, 
        fontWeight: 'normal', 
        textAlign: 'center', 
        alignSelf: 'center', 
        color: '#303030',
        marginBottom: 0,
        width: '100%',
    },
    navButtonText: {         
        fontSize: 20, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
        textDecorationLine: 'underline',
    },
})