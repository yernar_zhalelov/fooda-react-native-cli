import React, {useEffect, useState} from 'react'
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image, FlatList, ToastAndroid } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { screenWidth, screenHeight } from '../utils/Dimentions'

import { FormButtonSmall } from '../components/FormButtomSmall'
import { url } from '../screens/AppConfig'


export const MenuQuestionModal = ({visible, onClose}) => {
    const [marks, setMarks] = useState([])

    useEffect(() => {
        AsyncStorage.getItem('token').then((token) => {

            fetch(url + `/api/get/designation`, {
                method: 'GET', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {

                let qualities = responseJson.data.filter(e => e.catalog == 'Качества')
                qualities = qualities.sort((a, b) => a.name.localeCompare(b.name))

                let vivod = responseJson.data.filter(e => e.catalog == 'Вывод')
                vivod = vivod.sort((a, b) => a.icon.localeCompare(b.icon))
                

                let result = qualities.concat(vivod)
                
                setMarks(result)
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })

        })
    }, [])

    const renderMark = (item) => {

        return (
            <View style={{marginLeft: 20, flexDirection: 'row', marginTop: 15}}>
                <Image source={{uri: item.icon}} style={{width: 28, height: 25, resizeMode: 'contain'}} />
                <Text style={styles.textdark}>— {item.name}</Text>
            </View>
        )
    }

    return (
        <Modal visible={visible}
            animationType={'slide'}
            backdropColor='gray'
            transparent={true}
            visible={visible}
            onBackdropPress={()=>setVisibility(false)}>
            <View style={styles.modalBackground}>
                <View style={{width: screenWidth * 0.85, height: screenWidth * 1.22, backgroundColor: '#fff', borderRadius: 20, borderColor: '#85B057', borderWidth: 1}}>
                    <TouchableOpacity 
                        style={{position: 'absolute', right: 10, top: 10}}
                        onPress={onClose}>
                        <Image source={require('../assets/images/icoClose.png')} style={{width: 28, height: 28}} />
                    </TouchableOpacity>
                    <Text style={styles.navButtonText2}>Обозначения</Text>

                    <FlatList 
                        data={marks}
                        renderItem={({item}) => renderMark(item)}
                        style={{
                            
                        }}
                        showsVerticalScrollIndicator={true}
                    />

                    <View style={{marginTop: 5, marginBottom: 5}}>
                        <FormButtonSmall
                            onPress={onClose}
                            buttonTitle='Понятно'
                        />
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.8)', 
    },
    navButtonText2: {         
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
        alignSelf: 'center',
        marginTop: 25,
    },
    textdark: {
        marginTop: 5,
        marginLeft: 20,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
        width: '80%',
    },
})