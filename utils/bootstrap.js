import * as Font from 'expo-font'

export async function bootstrap() {
    await Font.loadAsync({
       'roboto-bold': require('../assets/fonts/Roboto-Bold.ttf'),
       'roboto-regular': require('../assets/fonts/Roboto-Regular.ttf'),
       'montserrat-regular': require('../assets/fonts/Montserrat-Regular.ttf'),
       'montserrat-bold': require('../assets/fonts/Montserrat-Bold.ttf'),
       'montserrat-extrabold': require('../assets/fonts/Montserrat-ExtraBold.ttf')
    })
}