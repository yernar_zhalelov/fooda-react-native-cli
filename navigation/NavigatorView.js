import React from 'react';
import { SafeAreaView } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';

import { createStackNavigator } from '@react-navigation/stack';
import { MainScreen } from '../screens/MainScreen';
import { LoginScreen } from '../screens/LoginScreen';
import { RegisterScreen } from '../screens/RegisterScreen';
import { ProductDetailsScreen } from '../screens/ProductDetailsScreen'
import Tabs from './BottomTabNavigator';

const stackData = [
    {
        name: "Main", 
        component: MainScreen
    },
    {
        name: "Login", 
        component: LoginScreen
    },
    {
        name: "Register", 
        component: RegisterScreen
    },
    {
        name: "Tabs", 
        component: Tabs
    },
    {
        name: 'ProductDetails', 
        component: ProductDetailsScreen
    }
]

const ParentStack = createStackNavigator();


export default function NavigatorView(props) {

    return (
        // <SafeAreaView style={{
        //     flex: 1, 
        //     ...ifIphoneX({
        //         marginTop: -35
        //     })
        // }}>
        <ParentStack.Navigator>
            {stackData.map((item, index) => (
                <ParentStack.Screen 
                    name={item.name}
                    component={item.component}
                    options={({route}) => ({
                        headerTransparent: true,
                        headerLeft: null,
                        headerTitle: '', 
                    })}
                />
            ))}
        </ParentStack.Navigator>
        // </SafeAreaView>
    )
}