import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { CategoryScreen } from "../screens/CategoryScreen";
import { MainScreen } from "../screens/MainScreen";
import { MenuScreen } from "../screens/MenuScreen";
import { SubCategoryScreen } from "../screens/SubCategoryScreen";
import { SubSubCategoryScreen } from "../screens/SubSubCategoryScreen";
import { ProductDetailsScreen } from "../screens/ProductDetailsScreen";

const MainData = [
    {
        name: "Menu", 
        component: MenuScreen
    },
    {
        name: "SubCategory", 
        component: SubCategoryScreen
    },
    {
        name: "SubSubCategory", 
        component: SubSubCategoryScreen
    },
    {
        name: "Category", 
        component: CategoryScreen
    },
    {
        name: "ProductDetails", 
        component: ProductDetailsScreen
    }
]

const MainStack = createStackNavigator()

const MainView = () => {
    return (
        <MainStack.Navigator>
            {
                MainData.map((item, index) => (
                    <MainStack.Screen 
                        name={item.name} 
                        component={item.component}

                        options={({ route }) => ({
                            headerTransparent: true,
                            headerTitle: ' ',
                            headerLeft: null,
                        })}
                    />
                ))
            }
        </MainStack.Navigator>
    )
}

export default MainView