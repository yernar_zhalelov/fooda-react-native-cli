import React from 'react';
import {View, Text, SafeAreaView} from 'react-native'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { AccountScreen } from "../screens/AccountScreen";
import { AddProductScreen } from "../screens/AddProductScreen";
import { FavoritesScreen } from "../screens/FavoritesScreen";
import { SearchScreen } from "../screens/SearchScreen";
import { ProductDetailsScreen } from '../screens/ProductDetailsScreen'
import MainView from "./MainStack";

import IcoTab1 from '../assets/svg/IcoTab1';
import IcoTab2 from '../assets/svg/IcoTab2';
import IcoTab3 from '../assets/svg/IcoTab3';
import IcoTab4 from '../assets/svg/IcoTab4';
import IcoTab5 from '../assets/svg/IcoTab5';
import { ifIphoneX } from 'react-native-iphone-x-helper';

const mainStack = createStackNavigator()
const searchStack = createStackNavigator()
const favStack = createStackNavigator()

// function mainStackFunction() {
//     return (
//         <mainStack.Navigator>
//             <mainStack.Screen 
//                 name="Main"
//                 component={MainView}
//                 options={({route}) => ({
//                     headerTransparent: true, 
//                     headerTitle: '', 
//                     headerLeft: null
//                 })}
//             />
//             <mainStack.Screen 
//                 name="ProductDetails"
//                 component={ProductDetailsScreen}
//                 options={({route}) => ({
//                     headerTransparent: true, 
//                     headerTitle: '',
//                     headerLeft: null
//                 })}
//             />
//         </mainStack.Navigator>
//     )
// }

function searchStackFunction() {
    return (
        <searchStack.Navigator>
            <searchStack.Screen 
                name="Search"
                component={SearchScreen}
                options={({route}) => ({
                    headerTransparent: true, 
                    headerTitle: '',
                    headerLeft: null
                })}
            />
            <searchStack.Screen 
                name="ProductDetails"
                component={ProductDetailsScreen}
                options={({route}) => ({
                    headerTransparent: true, 
                    headerTitle: '',
                    headerLeft: null
                })}
            />
        </searchStack.Navigator>
    )
}

function favoritesStackFunction() {
    return (
        <favStack.Navigator>
            <favStack.Screen 
                name="Favorites"
                component={FavoritesScreen}
                options={({route}) => ({
                    headerTransparent: true, 
                    headerTitle: '',
                    headerLeft: null
                })}
            />
            <favStack.Screen 
                name="ProductDetails"
                component={ProductDetailsScreen}
                options={({route}) => ({
                    headerTransparent: true, 
                    headerTitle: '',
                    headerLeft: null
                })}
            />
        </favStack.Navigator>
    )
}

const bottomDataNavigator = [
    {
        name: "Menu", 
        component: MainView, 
        icon: IcoTab1
    },
    {
        name: "Search", 
        component: searchStackFunction, 
        icon: IcoTab2
    },
    {
        name: "AddProduct", 
        component: AddProductScreen, 
        icon: IcoTab3
    },
    {
        name: "Favorites", 
        component: favoritesStackFunction, 
        icon: IcoTab4
    },
    {
        name: "Account", 
        component: AccountScreen, 
        icon: IcoTab5
    },
]

const Tab = createBottomTabNavigator();

const Tabs = () => {
    return(
        <SafeAreaView style={{
            flex: 1, 
            backgroundColor: '#fff',
            ...ifIphoneX({
                marginTop: -35, 
            })
        }}>
            <Tab.Navigator
                tabBarOptions={{
                    style: {
                        backgroundColor: '#fff', 
                        borderTopColor: '#fff', 
                        ...ifIphoneX({
                            marginBottom: -35
                        })
                    }
                }}
            >

                {bottomDataNavigator.map((item, index) => (
                    <Tab.Screen 
                        name={item.name}
                        component={item.component}
                        options={{
                            tabBarIcon: item.icon, 
                            tabBarLabel: ({focused}) => (
                                null
                            )
                        }}

                    />
                ))}

            </Tab.Navigator>
        </SafeAreaView>
    )
}

export default Tabs