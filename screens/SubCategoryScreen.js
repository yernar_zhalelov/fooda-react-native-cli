import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Dimensions, ScrollView, LogBox, ToastAndroid } from 'react-native'
import { bootstrap } from '../utils/bootstrap'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationBar } from '../components/NavigationBar'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { ProductItemSmall } from '../components/ProductItemSmall'
import { MenuItem } from '../components/MenuItem'
import { MenuQuestionModal } from '../modals/MenuQuestionModal'
import { Root } from '../components/Root'

import {url} from './AppConfig'


const numColumns1 = 3;

const numColumns2 = 2;

export const SubCategoryScreen = ({navigation, ...props}) => {
    const [products, setProducts] = useState(props.route.params.category)


    return <View style={styles.container}>
            <NavigationBar onPress={() => navigation.goBack()} screenTitle={props.route.params.title}/>

                <ScrollView>

                    

                    <View style={{width: '100%', height: '100%', alignSelf: 'center'}}>
                        <FlatList
                                data={products}
                                style={styles.flatlist}
                                renderItem={({item, index })=><MenuItem item={item} index={index} navigation={navigation} identifier='SubCategory'/>}
                                numColumns={numColumns2}
                                showsVerticalScrollIndicator={false}
                                // columnWrapperStyle={{ alignSelf: 'center',}}
                            />
                    </View>
                </ScrollView>
            </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    header: {
        height: 50, 
        marginTop: 10, 
        flexDirection: 'row', 
        alignItems: 'center', 
    },
    text: {
        marginTop: 50,
        marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 20, 
        fontWeight: '700', 
        textAlign: 'left', 
        color: '#303030',
    },
    flatlist: {
    },    
    navbutton: {
        width: 60,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center', 
        marginLeft: 10, 
        position: 'absolute', 
        zIndex: 10, 
        color: 'black', 
    },
    tilteText: {
        flex: 1
    }
})