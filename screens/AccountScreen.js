import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, ToastAndroid, Linking, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import moment from 'moment'
import 'moment/locale/ru'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { NavigationBar } from '../components/NavigationBar'
import { ChangePasswordModal } from '../modals/ChangePasswordModal'
import { ForgotPasswordModal } from '../modals/ForgotPasswordModal'
import { ResetPasswordModal } from '../modals/ResetPasswordModal'
import { PasswordChangedModal } from '../modals/PasswordChangedModal'
import { ManageSubscriptionModal } from '../modals/ManageSubscriptionModal'
import { SubscriptionOffConfirmModal } from '../modals/SubscriptionOffConfirmModal'
import { SubscriptionOffModal } from '../modals/SubscriptionOffModal'
import { Menu } from '../components/Menu'
import { Root } from '../components/Root'

import {url} from './AppConfig'

const screenWidth = Dimensions.get('screen').width
const screenHeight = Dimensions.get('screen').height

export const AccountScreen = ({navigation}) => {
    const [user, setUser] = useState('')

    useEffect(() => {

        AsyncStorage.getItem('user').then((user) => {

            setUser(JSON.parse(user))
        })
        
    }, [])

    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [newConfirmationPassword, setNewConfirmationPassword] = useState('')

    const [changePasswordModal, setChangePasswordModal] = useState(false)
    const [forgotPasswordModal, setForgotPasswordModal] = useState(false)
    const [resetPasswordModal, setResetPasswordModal] = useState(false)
    const [passwordChangedModal, setPasswordChangedModal] = useState(false)
    const [manageSubscriptionModal, setManageSubscriptionModal] = useState(false)
    const [subscriptionOffConfirmModal, setSubscriptionOffConfirmModal] = useState(false)
    const [subscriptionOffModal, setSubscriptionOffModal] = useState(false)

    const handleClick=()=>{ 
        setForgotPasswordModal(false)
        setResetPasswordModal(true)
    }

    const handleFPClick=()=>{ 
        setChangePasswordModal(false)
        setForgotPasswordModal(true)
    }

    const handlePChClick=()=>{ 
        changePassword()
    }

    const handleDetailsSubClick=()=>{ 
        setManageSubscriptionModal(false)
        setSubscriptionOffConfirmModal(true)
    }

    const handleUndoSubSubmitClick=()=>{ 
        setSubscriptionOffConfirmModal(false)
        setSubscriptionOffModal(true)
    }

    const handleUndoSubDiscardClick=()=>{ 
        setSubscriptionOffConfirmModal(false)
    }

    const renderMonth = () => {
        let current_date = new Date()

        moment.locale('ru')
        current_date = moment(current_date.setMonth(current_date.getMonth() + 1)).format('Do MMMM')

        return current_date.split(' ')[1]
    }

    const logout = () => {
        console.log("IAJDO")

        AsyncStorage.getItem('token').then((token) => {
            fetch(url + `/api/logout?token=Bearer ${token}`, {
                method: 'POST', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {
                console.log(responseJson)

                AsyncStorage.removeItem('token')
                AsyncStorage.removeItem('id')
                AsyncStorage.removeItem('allProducts')
                AsyncStorage.removeItem('favourites')
                AsyncStorage.removeItem('user')
                navigation.reset({
                    index: 0,
                    routes: [{name: 'Main'}],
                })
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })
        })

    }

    const changePassword = () => {
        if(oldPassword == '' || newPassword == '' || newConfirmationPassword == '') {
            ToastAndroid.show("Проверьте введенные данные", ToastAndroid.SHORT)
        } else {
            if(newPassword != newConfirmationPassword) {
                ToastAndroid.show("Пароли не совпадают", ToastAndroid.SHORT)
            } else {
                AsyncStorage.getItem('token').then((token) => {
                    fetch(url + `/api/change/password?oldPassword=${oldPassword}&newPassword=${newPassword}&newPassword_confirmation=${newConfirmationPassword}`, {
                        method: 'POST', 
                        headers: {
                            'Authorization': `Bearer ${token}`
                        },
                    }).
                    then((response) => response.json()).
                    then((responseJson) => {

                        if(responseJson.success == false) {
                            ToastAndroid.show(responseJson.data, ToastAndroid.SHORT)
                        } else {
                            setChangePasswordModal(false)
                            setPasswordChangedModal(true)
                        }
                    }).
                    catch((error) => {
                        console.log(JSON.stringify(error))
                    })
                })
            }
        }
    }

    const openUrl = () => {
        Linking.canOpenURL('https://api.whatsapp.com/send/?phone=79095496086').then((result) => {
            if(result == true) {
                Linking.openURL('https://api.whatsapp.com/send/?phone=79095496086')
            } else {
                ToastAndroid.show('Ошибка', ToastAndroid.SHORT)
            }
        })
    }

    const openSubUrl = () => {
        Linking.canOpenURL('http://voitenko.online').then((result) => {
            if(result == true) {
                Linking.openURL('http://voitenko.online')
            } else {
                ToastAndroid.show('Ошибка', ToastAndroid.SHORT)
            }
        })
    }

    return <View style={styles.container}>
            <NavigationBar onPress={() => navigation.goBack()} screenTitle='Ваш профиль' />

            <ScrollView>
                <ChangePasswordModal 
                    visible={changePasswordModal}
                    onSubmit={()=>handlePChClick()}
                    onForgot={()=>handleFPClick()}
                    onClose={()=>setChangePasswordModal(false)} 
                    oldPassword={oldPassword}
                    newPassword={newPassword}
                    newPasswordConfirmation={newConfirmationPassword}
                    setOld={(text) => setOldPassword(text.trim())}
                    setNew={(text) => setNewPassword(text.trim())}
                    setConfirmationNew={(text) => setNewConfirmationPassword(text.trim())}/>
                <ForgotPasswordModal 
                    visible={forgotPasswordModal}
                    onSubmit={()=>handleClick()}
                    onClose={()=>setForgotPasswordModal(false)} />
                <ResetPasswordModal 
                    visible={resetPasswordModal} 
                    onClose={()=>setResetPasswordModal(false)} />
                <PasswordChangedModal 
                    visible={passwordChangedModal} 
                    onClose={()=>setPasswordChangedModal(false)} />
                <ManageSubscriptionModal 
                    visible={manageSubscriptionModal}
                    onSubmit={()=>handleDetailsSubClick()}
                    onClose={()=>setManageSubscriptionModal(false)} />
                <SubscriptionOffConfirmModal 
                    visible={subscriptionOffConfirmModal}
                    onSubmit={()=>handleUndoSubSubmitClick()}
                    onDiscard={()=>handleUndoSubDiscardClick()}
                    onClose={()=>setSubscriptionOffConfirmModal(false)} /> 
                <SubscriptionOffModal 
                    visible={subscriptionOffModal} 
                    onClose={()=>setSubscriptionOffModal(false)} />  

                <View style={{
                    flexDirection: 'row', 
                    justifyContent: 'space-between'
                }}>

                    <View style={{
                        flex: 10
                    }}>
                        <Text style={styles.textstart}>Старт 1 {renderMonth()}</Text>
                        <Text style={styles.textmaraphon}>МАРАФОН СТРОЙНОСТИ</Text>
                        <Text style={styles.textbody}>Твоё идеальное тело в надёжных руках</Text>
                        <TouchableOpacity style={styles.buttonContainer} onPress={() => openSubUrl()}>
                            <Text style={styles.buttonText}>ЗАПИСАТЬСЯ</Text>
                        </TouchableOpacity>
                    </View>


                    <Image source={require('../assets/images/sportgirl.png')} style={styles.icon} />

                </View>

                
                

                <View style={{marginTop: 100}}>
                    <Text style={styles.textlight}>Email</Text>
                    <Text style={styles.textdark}>{user.email}</Text>
                    <Text style={styles.textsub}>Подписка активирована</Text>
                    <TouchableOpacity 
                        style={{width: '100%', marginTop: 20, marginBottom: 15}}
                        onPress={()=>setChangePasswordModal(true)}>
                        <Text style={styles.navButtonText}>Изменить пароль</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        style={{width: '100%', marginTop: 0, marginBottom: 15}}
                        onPress={()=>setManageSubscriptionModal(true)}>
                        <Text style={styles.navButtonText}>Управление подпиской</Text>
                    </TouchableOpacity>
                    <Text style={styles.boldDarkText}>Написать в поддержку:</Text>
                    <TouchableOpacity 
                            style={{width: 40, height: 40, marginLeft: 25, justifyContent: 'center', alignItems: 'center', marginTop: 5, marginBottom: 15}}
                            onPress={() => openUrl()}>
                            <Image style={{tintColor: '#86b05a', height: 40, width: 40}} source={require('../assets/images/icoSupport2.png')} />            
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={{marginLeft: 25}}
                        onPress={() => logout()}
                    >
                        <Text style={{   
                            fontSize: 16, 
                            fontWeight: 'bold',
                            textDecorationLine: 'underline',
                            color: '#ACABAB',
                            fontFamily: 'roboto-regular',
                        }}>
                            Выйти
                        </Text>
                    </TouchableOpacity>
                </View>

                
                <View>
                
                <View style={{width: screenWidth/5, height: 90}}>
  
                </View>
            </View>
        </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    navbar: {
        marginTop: 30,
        height: screenHeight * 0.08,
        justifyContent: 'center',
        alignItems: 'center',
    },
    navtext: {
        fontFamily: 'roboto-regular', 
        fontSize: 20, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',        
    },
    navbutton: {
        width: 40,
        height: 40, 
        position: 'absolute',
        left: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textlight: {
        marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'left',
        color: '#ACABAB',
        marginTop: 5,
        marginBottom: 5,
    },
    textdark: {
        marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },
    textsub: {
        marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#ACABAB',
        marginTop: 5,
        marginBottom: 5,
        width: '70%'
    },
    navButtonText: {    
        marginLeft: 25,     
        fontSize: 16, 
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        color: '#ACABAB',
        fontFamily: 'roboto-regular',
    },
    boldDarkText: {    
        marginTop: 10,
        marginLeft: 25,     
        fontSize: 16, 
        fontWeight: 'bold',
        color: '#303030',
        fontFamily: 'roboto-regular',
    },
    buttonContainer: {
        marginTop: 5,
        marginBottom: 10,
        marginLeft: 25, 
        width: screenWidth * 0.35, 
        height: screenHeight * 0.042,
        backgroundColor: '#EF5448',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,
     },
     buttonText: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#ffffff',
        fontFamily: 'montserrat-extrabold',
     },
     icon: {
        flex: 4,
        right: 2,
        width: screenWidth * 0.25, 
        height: screenWidth * 0.25, 
        resizeMode: 'contain'
    },
    textstart: {
        marginLeft: 25,
        fontFamily: 'montserrat-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left',
        color: '#EF5448',
        marginTop: 5,
        marginBottom: 5,
    },
    textmaraphon: {
        marginLeft: 25,
        fontFamily: 'montserrat-extrabold', 
        fontSize: 16, 
        fontWeight: '900', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },
    textbody: {
        marginLeft: 25,
        fontFamily: 'montserrat-regular', 
        fontSize: 11, 
        fontWeight: '400', 
        textAlign: 'left',
        color: '#ACABAB',
        marginTop: 5,
        marginBottom: 5,
    },
    textPrivate: {
        marginTop: 0,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 0,
    },
})