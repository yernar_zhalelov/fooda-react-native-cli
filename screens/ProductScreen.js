import React, {useState} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Dimensions, Image } from 'react-native'

import { NavigationBar } from '../components/NavigationBar'
import { SearchInput } from '../components/SearchInput'
import { ProductItem } from '../components/ProductItem'
import { Root } from '../components/Root'

const screenWidth = Dimensions.get('screen').width

const data = [
    {   
        key: 1, 
        image:require('../assets/images/items/item1.png'),
        like:require('../assets/images/svg/icoLike.svg'),
    },
    {   
        key: 2, 
        image:require('../assets/images/items/item2.png'),
        like:require('../assets/images/svg/icoDislike.svg'),
    },
    {   
        key: 3, 
        image:require('../assets/images/items/item3.png'),
        like:require('../assets/images/svg/icoNolike.svg'),
    },
    {   
        key: 4, 
        image:require('../assets/images/items/item4.png'),
        like:require('../assets/images/svg/icoLike.svg'),
    },
    {   
        key: 5, 
        image:require('../assets/images/items/item5.png'),
        like:require('../assets/images/svg/icoDislike.svg'),
    },
    {   
        key: 6, 
        image:require('../assets/images/items/item6.png'),
        like:require('../assets/images/svg/icoNolike.svg'),
    },
    {   
        key: 7, 
        image:require('../assets/images/items/item1.png'),
        like:require('../assets/images/svg/icoLike.svg'),
    },
    {   
        key: 8, 
        image:require('../assets/images/items/item2.png'),
        like:require('../assets/images/svg/icoDislike.svg'),
    },
    {   
        key: 9, 
        image:require('../assets/images/items/item3.png'),
        like:require('../assets/images/svg/icoNolike.svg'),
    },
  ];

const numColumns = 2;

export const ProductScreen = ({route, navigation}) => {
    const [category, setCategory] = useState(route.params.category)
    const [products, setProducts] = useState(route.params.products)

    return <Root style={styles.container}>
                <NavigationBar onPress={() => navigation.goBack()} screenTitle={category}/>
                <SearchInput placeholderText='Поиск продукта' />
                <FlatList
                    data={products}
                    style={styles.flatlist}
                    renderItem={({item, index })=><ProductItem item={item} index={index} navigation={navigation} />}
                    numColumns={numColumns}
                />
                <View style={{width: screenWidth/5, height: 72}}>
  
                </View>
            </Root>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
    },
    flatlist: {
        padding: 20,
    },
    item: {
        width: '100%', 
        height: '100%',
        backgroundColor: '#fff',

        flex: 1,
        margin: 1,
        height: Dimensions.get('screen').width / numColumns, // approximate a square
    },
    item2: {
        borderColor: '#000',
        width: '100%', 
        height: '80%', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemTitle: {
        marginLeft: 3,
        fontFamily: 'roboto-regular', 
        fontSize: 10, 
        color: '#000',
    },
    image: {
        height: '80%',
        resizeMode: 'contain',
    },
})