import React, { useEffect, useState } from 'react'
import { View, KeyboardAvoidingView, Text, StyleSheet, Image, Button, TextInput, TouchableOpacity, Modal, ToastAndroid, Alert, Platform, ScrollView, Dimensions, SafeAreaView} from 'react-native'
import { color } from 'react-native-reanimated'
import { FormInput } from '../components/FormInput'
import { FormButton } from '../components/FormButton'
import { ForgotPasswordModal } from '../modals/ForgotPasswordModal'
import { ResetPasswordModal } from '../modals/ResetPasswordModal'
import Toast from 'react-native-root-toast';

import {url} from './AppConfig'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const screenWidth = Dimensions.get('screen').width

export const LoginScreen = ({navigation}) => {
    useEffect(() => {
        AsyncStorage.getItem("token").then((data) => {
            if(data != null) {
                navigation.reset({
                    index: 0,
                    routes: [{name: 'Menu'}],
                  })
            }
        })
    })

    const [forgotPasswordModal, setForgotPasswordModal] = useState(false)
    const [resetPasswordModal, setResetPasswordModal] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [forgetEmail, setForgetEmail] = useState('')

    const handleClick = ()=> { 
        setForgotPasswordModal(false)
        setResetPasswordModal(true)
    }

    const forgetPassword = () => {
        if(forgetEmail == '') {
            console.log("eror no email")
        } else {
            
           
            fetch(url + `/api/password/forgot?email=${forgetEmail}`, {
                method: 'POST', 
                headers: {},
            }).
            then((response) => response.json()). 
            then((responseJson) => {
                if(responseJson.msg == 'Reset password link sent on your email id.') {
                    
                    Toast.show('Сообщение для сброса пароля отправлено на Вашу почту', {
                        duration: Toast.durations.SHORT, 
                    })
                    handleClick()
                    
                }
            }).
            catch((error) => {
                if(Platform.OS == 'android') {
                    console.log(JSON.stringify(error))
                    handleClick()
                } else {
                    Alert.alert(error)
                }
            })
            
            
        }
    }

    const loginUser = () => {
        if(email == '' || password == '') {
            Toast.show('Проверьте введенные данные', {
                duration: Toast.durations.SHORT, 
                position: Toast.positions.CENTER
            })
        } else {
            fetch(url + `/api/login?email=${email}&password=${password}`, {
                method: 'POST', 
                headers: {},
            }).
            then((response) => response.json()).
            then((responseJson) => {
                if(responseJson.token) {
                    AsyncStorage.setItem('token', responseJson.token)
                    getData(responseJson.token)
                    AsyncStorage.setItem('id', responseJson.role_id.toString())
                }
                
                if(responseJson.error != undefined) {
                    Toast.show('Проверьте введенные данные', {
                        duration: Toast.durations.SHORT, 
                        position: Toast.positions.CENTER
                    })
                }
                

            }).
            catch((error) => {
                if(Platform.OS == 'android') {
                    console.log(JSON.stringify(error))
                } else {
                    Alert.alert(error)
                }
            })
        }
    }

    const getData = (token) => {
        fetch(url + `/api/get/all/product`, {
            method: 'GET', 
            headers: {
                'Authorization': `Bearer ${token}`
            },
        }).
        then((response) => response.json()).
        then((responseJson) => {
    
            AsyncStorage.setItem('allProducts', JSON.stringify(responseJson.data))
    
            setTimeout(() => {
                navigation.replace('Tabs')
            }, 100)

        }).
        catch((error) => {
            ToastAndroid.show(JSON.stringify(error), ToastAndroid.LONG)
        })
        
        //DIVIDER

        fetch(url + `/api/show/chosen?token=Bearer ${token}`, {
            method: 'GET', 
            headers: {
                'Authorization': `Bearer ${token}`
            },
        }).
        then((response) => response.json()).
        then((responseJson) => {
            AsyncStorage.setItem('favourites', JSON.stringify(responseJson.data))
        }).
        catch((error) => {
            console.log(JSON.stringify(error))
        })
    }

    return <SafeAreaView style={{
        flex: 1, 
        // ...ifIphoneX({marginTop: -35}),
        backgroundColor: '#fff'
        }}>
    <ScrollView keyboardShouldPersistTaps='handled' style={{flex: 1, backgroundColor: '#fff'}}>
        <KeyboardAvoidingView behavior='position'
            style={styles.container}
            >
            
        <ForgotPasswordModal 
            visible={forgotPasswordModal}
            onSubmit={()=>forgetPassword()}
            onClose={()=>setForgotPasswordModal(false)} 
            email={forgetEmail}
            changeText={(text) => setForgetEmail(text.trim())}
            />
        <ResetPasswordModal 
            visible={resetPasswordModal} 
            onClose={()=>setResetPasswordModal(false)} />           

        <Image source={require('../assets/images/woman.png')} style={styles.logo}>
        </Image>
        <Text style={styles.text}>FOODA</Text>
        <Text style={styles.text2}>Приложение с разбором продуктов</Text>
        <FormInput
            placeholderText='E-mail' 
            iconType='mail'
            labelValue={email}
            changeText={(text) => setEmail(text.trim())}
        />
        <FormInput
            placeholderText='Пароль' 
            iconType='password'
            labelValue={password}
            changeText={(text) => setPassword(text.trim())}
        />
        <TouchableOpacity 
            style={{width: '100%', alignItems: 'center', marginTop: 5, marginBottom: 10}}
            onPress={()=>setForgotPasswordModal(true)}>
            <Text style={styles.textPrivate}>Забыли пароль?</Text>
        </TouchableOpacity>
        <FormButton
            buttonTitle='АВТОРИЗИРОВАТЬСЯ'
            onPress={()=>loginUser()} 
        />
        <TouchableOpacity 
            style={{width: '100%', alignItems: 'center', marginTop: 20, marginBottom: 15}}
            onPress={()=>navigation.navigate('Register')}>
            <Text style={styles.navButtonText}>Регистрация аккаунта</Text>
        </TouchableOpacity>
    </KeyboardAvoidingView>
    </ScrollView>
    </SafeAreaView>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
        flex: 1,
    },
    text: {
        fontFamily: 'roboto-bold', 
        fontSize: 32, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#85B057',        
    },
    text2: {
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginTop: 5,
        marginBottom: 35,
    },
    textPrivate: {
        fontFamily: 'roboto-regular',
        flexDirection: 'row', 
        flexWrap: 'wrap',
        justifyContent: 'center',
        color: '#ACABAB',
        fontSize: 14, 
    },
    navButtonText: {         
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#303030',
        fontFamily: 'roboto-regular',
    },
    center: {
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
    },
    input: {
        width: 250,
        height: 40,
        borderWidth: 1,
        borderColor: '#ACABAB',
        borderRadius: 10,
        color: '#5E5E5E',
    },
    logo: {        
        width: screenWidth * 0.8,
        height: screenWidth * 0.8,
        marginLeft: screenWidth * 0.1,
        marginTop: screenWidth * 0.1,
        marginBottom: screenWidth * 0.05,
    },
})