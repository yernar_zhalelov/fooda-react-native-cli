import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Dimensions, ScrollView, ToastAndroid, TouchableHighlight } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { ProductItemSmall } from '../components/ProductItemSmall'
import { MenuItem } from '../components/MenuItem'
import { MenuQuestionModal } from '../modals/MenuQuestionModal'
import { Root } from '../components/Root'

import {url} from './AppConfig'

const screenWidth = Dimensions.get('screen').width

const numColumns1 = 3;

const numColumns2 = 2;

export const MenuScreen = ({navigation}) => {
    const [menuQuestionModal, setMenuQuestionModal] = useState(false)
    const [newInParsing, setNewInParsing] = useState('')
    const [categories, setCategories] = useState([])

    function getData () {
        AsyncStorage.getItem('token').then((token) => {
            console.log("token = ", token)

            fetch(url + `/api/new/parsing`, {
                method: 'GET', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then(responseJson => {
                console.log(responseJson.data)

                AsyncStorage.getItem('allProducts').then((data) => {

                    let result =[]
                    let parsing = responseJson.data.filter(e => e.name === "Опубликованные")
                    let all = JSON.parse(data)

                    parsing.forEach(element => {
                        let temp = all.find(e => element.nameProduct == e.nameProduct)

                        result.push(temp)

                    });


                    setNewInParsing(result)
                })

                
                
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })

            //aisdufhiusdhfiuh

            fetch(url + `/api/get/category/tree`, {
                method: 'GET', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {
                
                setCategories(responseJson.data)

            }). 
            catch((error) => {
                console.log(JSON.stringify(error))
            })

            //iuasdfhiusdahf
            
            fetch(url + `/api/profile?token=Bearer ${token}`, {
                method: 'GET', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {

                AsyncStorage.setItem('user', JSON.stringify(responseJson.data))
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })
        })
    }

    function getOldData () {

        AsyncStorage.getItem('token').then((token) => {
            
            fetch(url + `/api/show/chosen?token=Bearer ${token}`, {
                method: 'GET', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {
                AsyncStorage.setItem('favourites', JSON.stringify(responseJson.data))
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })
            
        })
    }

    useEffect(() => {
        getData()

        const unsubscribe = navigation.addListener('focus', () => {
            getOldData()
        })
        
        return unsubscribe
    },[navigation])



    return <View style={styles.container}>
                <View style={styles.header}>

                    <Text style={styles.text}>Новое в разборе</Text>

                    <TouchableOpacity
                        onPress={()=>setMenuQuestionModal(true)}
                        style={{
                            width: 50, 
                            height: 50,
                            alignItems: 'center', 
                            justifyContent: 'center'
                        }}
                    >

                        <Image source={require('../assets/images/icoQuestion2.png')} style={{width: 25, height: 25, tintColor: '#85b057', }} />
                        
                    </TouchableOpacity>

                </View>

                <ScrollView>
                    <MenuQuestionModal 
                        visible={menuQuestionModal} 
                        onClose={()=>setMenuQuestionModal(false)} 
                    />  
        
                    
                    <View style={{width: '100%', alignSelf: 'center', marginTop: 20, marginLeft: 10, marginRight: 3, paddingBottom: 5, }}>
                        <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                horizontal={true}
                                data={newInParsing}
                                style={styles.flatlistParsing}
                                showsHorizontalScrollIndicator={false}
                                persistentScrollbar={false}
                                renderItem={({item, index })=><ProductItemSmall item={item} index={index} navigation={navigation}/>}
                            />
                    </View>

                    <View
                        style={{
                            marginTop: 10,
                            borderBottomColor: '#dfdfdf',
                            borderBottomWidth: 2,
                        }}
                    />

                    <View style={{width: '100%', height: '100%', alignSelf: 'center',}}>
                        <FlatList
                            keyExtractor={(item, index) => index.toString()}
                            data={categories}
                            style={styles.flatlistMenu}
                            renderItem={({item, index })=><MenuItem item={item} index={index} navigation={navigation} identifier="Menu"/>}
                            numColumns={numColumns2}
                            columnWrapperStyle={{
                                alignSelf: 'center', 
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
    },
    text: {
        // marginTop: 20,
        // marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 20, 
        fontWeight: '700', 
        // textAlign: 'left', 
        color: '#303030',
        alignSelf: 'center'
    },
    flatlistParsing: {
        
    },  
    flatlistMenu: {
        marginBottom: 40, 

    }, 
    header: {
        flexDirection: 'row', 
        width: '90%', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        alignSelf: 'center', 
        ...ifIphoneX({
            marginTop: 35
        }),
    }
})