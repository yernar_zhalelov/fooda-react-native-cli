import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, Dimensions, ScrollView, LogBox, ToastAndroid } from 'react-native'
import { bootstrap } from '../utils/bootstrap'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { ProductItemSmall } from '../components/ProductItemSmall'
import { MenuItem } from '../components/MenuItem'
import { MenuQuestionModal } from '../modals/MenuQuestionModal'
import { Root } from '../components/Root'
import { NavigationBar } from '../components/NavigationBar'

import {url} from './AppConfig'
import { ProductItem } from '../components/ProductItem'


const numColumns1 = 3;

const numColumns2 = 2;

export const SubSubCategoryScreen = ({navigation, ...props}) => {
    const [menuQuestionModal, setMenuQuestionModal] = useState(false)
    const [products, setProducts] = useState([])
    const [allProducts, setAllProducts] = useState([])
    const [item, setItem] = useState(props.route.params.item)

    useEffect(() => {

        AsyncStorage.getItem('allProducts').then((products) => {
            var result = []
            var all = JSON.parse(products)

            all.forEach(element => {
                if(element.category_id == item.id) {
                    result.push(element)
                }
            });

            
            setProducts(result)
           
        })
    },[])


    return <View style={styles.container}>
             <NavigationBar onPress={() => navigation.goBack()} screenTitle={props.route.params.title} identificator={true} openModal={() => setMenuQuestionModal(true)}/>

                <ScrollView>
                    <MenuQuestionModal 
                        visible={menuQuestionModal} 
                        onClose={()=>setMenuQuestionModal(false)} />  

                   

                    <View style={styles.flatlist}>
                        <FlatList
                            data={products}
                            style={{
                                width: '100%',
                                alignSelf: 'center', 
                            }}
                            renderItem={({item, index })=><ProductItem item={item} index={index} navigation={navigation}/>}
                            numColumns={numColumns2}
                            showsVerticalScrollIndicator={false}
                            columnWrapperStyle={{
                                // alignSelf: 'center'
                                alignContent: 'center'
                            }}
                        />
                    </View>
                    
                    
                </ScrollView>
            </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    header: {
        height: 50, 
        marginTop: 10, 
        flexDirection: 'row', 
        alignItems: 'center', 
    },
    text: {
        marginTop: 50,
        marginLeft: 25,
        fontFamily: 'roboto-regular', 
        fontSize: 20, 
        fontWeight: '700', 
        textAlign: 'left', 
        color: '#303030',
    },
    flatlist: {
        alignSelf: 'center',
        width: '95%',
        marginBottom: 50, 
        justifyContent: 'center'
    },    
    navbutton: {
        width: 60,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center', 
        marginLeft: 10, 
        position: 'absolute', 
        zIndex: 10, 
        color: 'black',
    },
    tilteText: {
        flex: 1,
    } 
})