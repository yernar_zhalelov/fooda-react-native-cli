import React from 'react'
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'

import { NavigationBar } from '../components/NavigationBar'
import { SubscribeButton } from '../components/SubscribeButton'

export const SubscribeScreen = ({navigation}) => {
    return <View style={styles.container}>
        <NavigationBar onPress={() => navigation.goBack()} screenTitle='Оформление подписки' />
        <Text style={styles.textdark}>Выберите подписку:</Text>
        <SubscribeButton price='199 ₽' monthText='НА 1 МЕСЯЦ' onPress={()=>navigation.navigate('Login')} />
        <SubscribeButton price='499 ₽' monthText='НА 3 МЕСЯЦА' onPress={()=>navigation.navigate('Login')} />
        <SubscribeButton price='999 ₽' monthText='НА 6 МЕСЯЦЕВ' onPress={()=>navigation.navigate('Login')} />
        <Text style={styles.texttrial}>Попробуйте бесплатно в течение 7 дней!</Text>
        <Text style={styles.textdescription}>По истечении пробного периода</Text>
        <Text style={styles.textdescription}>произойдёт автоматическое продление</Text>
        <Text style={styles.textdescription}>оплаты за первый месяц подписки.</Text>
        <Text style={styles.textdescription}>Подписку можно отменить в любое время.</Text>
    </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
    },
    textdark: {
        marginTop: 10,
        marginLeft: 25,
        paddingBottom: 30,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },    
    texttrial: {
        marginTop: 50,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 35,
    },
    textdescription: {         
        fontSize: 14, 
        fontWeight: 'normal', 
        color: '#848199',
        textAlign: 'center',
        fontFamily: 'roboto-regular',
    },
})