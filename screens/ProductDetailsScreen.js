import React, {useState, useEffect, useLayoutEffect} from 'react'
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ScrollView, Alert, ToastAndroid, Linking, Share } from 'react-native'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'
import Toast from 'react-native-root-toast'

import { NavigationBar } from '../components/NavigationBar'
import { FormButtonSmall } from '../components/FormButtomSmall'
import { Root } from '../components/Root'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { MenuQuestionModal } from '../modals/MenuQuestionModal'

import { url } from './AppConfig'
import IcoHeart from '../assets/svg/IcoHeart'
import IcoHeartRed from '../assets/svg/IcoHeartRed'


const like = require('../assets/images/svg/icoLike.svg')
const dislike = require('../assets/images/svg/icoDislike.svg')
const nolike = require('../assets/images/svg/icoNolike.svg')

const getImage = (status) => {
    if (status == "Не рекомендую") {
        return dislike
    } else if (status == "Не могу рекомендовать") {
        return nolike
    } else if (status == "Рекомендую") {
        return like
    }
}

export const ProductDetailsScreen = ({route, navigation}) => {
    const [menuQuestionModal, setMenuQuestionModal] = useState(false)
    const [statusChosen, setStatusChosen] = useState(false)
    const [product, setProduct] = useState(route.params.product)
    const [id, setId] = useState(null)
    const [favs, setFavs] = useState()

    const onShare = async () => {

        try {
          const result = await Share.share({
            message: `https://fooda-app.ru?product=${product.nameProduct}`.split(' ').join('%20') 
          });
        } catch (error) {
          alert(error.message);
        }
      };

    useLayoutEffect(() => {

        const accId = AsyncStorage.getItem('id')
        setId(accId)

        AsyncStorage.getItem('allProducts').then((products) => {
            let result;
            
            if(product.nameProduct != undefined) {
                result = JSON.parse(products).find(e => e.nameProduct === product.nameProduct)

                setProduct(result)
            } else {
                result = JSON.parse(products).find(e => e.nameProduct === product)

                setProduct(result)
            }


        })

        AsyncStorage.getItem('favourites').then((favs) => {
            setFavs(JSON.parse(favs))

            if(product.nameProduct != undefined) {
                if(JSON.parse(favs).find(e => e.nameProduct == product.nameProduct)) {
                    setStatusChosen(true)
                }
            } else {
                if(JSON.parse(favs).find(e => e.nameProduct == product)) {
                    setStatusChosen(true)
                }
            }
            
        })
    }, [])


    function saveToChosen () {

        AsyncStorage.getItem('token').then((token) => {
            fetch(url + `/api/save/toChosen?product_id=${product.id}`, {
                method: 'POST', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {
                
                if(responseJson.success == true) {
                    favs.push(product)

                    AsyncStorage.setItem('favourites', JSON.stringify(favs))
                    setStatusChosen(true)

                    Toast.show('Продукт добавлен', {
                        position: Toast.positions.BOTTOM, 
                        duration: Toast.durations.SHORT
                    })

                } else {
                    Toast.show('Ошибка', {
                        position: Toast.positions.BOTTOM, 
                        duration: Toast.durations.SHORT
                    })
                }

            }). 
            catch((error) => {
                console.log(JSON.stringify(error))
            })
            
        })

        
    }

    function deleteFromChosen () {

        AsyncStorage.getItem('token').then((token) => {
            fetch(url + `/api/delete/fromChosen?product_id=${product.id}`, {
                method: 'POST', 
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).
            then((response) => response.json()).
            then((responseJson) => {

                if(responseJson.success == true) {
                    var deletingItem = favs.find(e => e.id == product.id)

                    favs.splice(favs.indexOf(deletingItem), 1)

                    AsyncStorage.setItem('favourites', JSON.stringify(favs))
                    setStatusChosen(false)

                    Toast.show('Продукт удален', {
                        position: Toast.positions.BOTTOM, 
                        duration: Toast.durations.SHORT
                    })
                } else {
                    Toast.show('Ошибка', {
                        position: Toast.positions.BOTTOM, 
                        duration: Toast.durations.SHORT
                    })
                }
            }). 
            catch((error) => {
                console.log(JSON.stringify(error))
            })
        })
    }
    
    const showAlert = () => {
        if(statusChosen == true) {
            deleteFromChosen()
        } else {
           saveToChosen()
        }
    }


    const calculatePercentFormula = (type) => {

        if(type == 'kcal') {
            return parseInt(product.kkal) + parseInt(product.belki)
        } 

        if(type == 'carbohydrat') {
            var temp = (parseInt(product.kkal) + parseInt(product.belki)) - (parseInt(product.belki) * 4 + parseInt(product.zhir * 9))

            return parseInt(temp) / 4
        }
    }

    const renderNezhelDobavki = () => {
        if(product.nezhelDobavki == null) {
            return null
        } else {
            return (
                <View style={{marginTop: 10}}>             
                    <Text style={styles.textlight}>Нежелательные добавки</Text>
                    <Text style={styles.textdark}>{product.nezhelDobavki}</Text>
                </View>
            )
        }
    }

    const renderDopOpisanie = () => {
        if(product.dopOpisanie == null) {
            return null
        } else {
            return (
                <View style={{marginTop: 10}}>             
                    <Text style={styles.textlight}>Дополнительное описание</Text>
                    <Text style={styles.textdark}>{product.dopOpisanie}</Text>
                </View>
            )
        }
    }

    const renderFact = () => {

        if(id == '2' || id == '1') {
            return (
                <View>
                    <Text style={styles.textPrivate2}>Факт:</Text>
                    <View style={{flexDirection: 'row', marginTop: 5, marginBottom:0, width: '90%', marginLeft: 10}}>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <Text style={styles.textlighttable}>Ккал</Text>
                            <Text style={styles.textdarktable}>{calculatePercentFormula('kcal')}</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <Text style={styles.textlighttable}>Белки</Text>
                            <Text style={styles.textdarktable}>{product.belki}</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <Text style={styles.textlighttable}>Жиры</Text>
                            <Text style={styles.textdarktable}>{product.zhir}</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <Text style={styles.textlighttable}>Углеводы</Text>
                            <Text style={styles.textdarktable}>{calculatePercentFormula('carbohydrat')}</Text>
                        </View>
                    </View>
                </View>
            )
        } else {
            return null
        }
            
    }

    const renderHeart = () => {

        if(statusChosen) {
            return (
                <TouchableOpacity style={{ alignSelf: 'flex-end'}} onPress={() => showAlert()}>
                    
                    <IcoHeartRed
                        width='42'
                        height='42'
                    />
                    
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={() => showAlert()}>
                    <IcoHeart
                        width='42'
                        height='42'
                    />
                </TouchableOpacity>
            )
        }
        
    }

    const renderSmallIcons = () => {

        let result = []
        let i = 1

        if(product.icon != undefined) {
            product.icon.forEach(element => {
                result.push(
                    <Image 
                    source={{uri: element.icon}}
                    style={{
                        width: 40, 
                        height: 40, 
                        resizeMode: 'contain', 
                    }}
                    />
                )

                i = i + 16
            });
        }

        
        return (
            <View style={{
                backgroundColor: '#fff', 
                height: '95%', 
                flex: 1,
                justifyContent: 'space-between', 
            }}>
                {result}
            </View>
        )

    }

    const renderVivodImage = () => {
        return (
            <Image 
                source={{uri: product.vivod_icon}}
                style={{
                    width: 47, 
                    height: 46, 
                    alignSelf: 'flex-end'
                }}
            />
        )
    }

    const openWpUrl = () => {
        Linking.canOpenURL('https://api.whatsapp.com/send/?phone=79095496086').then((result) => {
            if(result == true) {
                Linking.openURL('https://api.whatsapp.com/send/?phone=79095496086')
            } else {
                Toast.show('Ошибка', {
                    position: Toast.positions.BOTTOM, 
                    duration: Toast.durations.SHORT
                })
            }
        })
    }

    const renderGdekupit = () => {
        let result = []

        if(product.gdekupit != undefined) {
            if(product.gdekupit.length == 0) {
                return null
            } else {
                product.gdekupit.forEach((element) => {
                    result.push(
                        <Text style={styles.textdark}>{element.gde_kupit}</Text>
                    )
                });
        
                
                return (
                    <View style={{marginTop: 10}}>             
                        <Text style={styles.textlight}>Где купить:</Text>
                        {result}
                    </View>
                )
            }
        }

    }

    return <View style={styles.container}>
            <View 
                style={{
                    ...ifIphoneX({
                        marginTop: 30
                    })
                }}
            />

            <NavigationBar onPress={() => navigation.goBack()} screenTitle={product.nameProduct} identificator={true} openModal={() => setMenuQuestionModal(true)}/>

            <ScrollView style={{flex: 1, ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })}}>

                <MenuQuestionModal 
                    visible={menuQuestionModal} 
                    onClose={()=>setMenuQuestionModal(false)} 
                />  

                

                <View style={styles.item}>
                    <View style={styles.item2}>
                        {renderSmallIcons()}

                        <Image source={{uri: product.imgProduct}} style={styles.image}/>

                        <View style={{
                            backgroundColor: '#fff',
                            height: '95%', 
                            justifyContent: 'space-between', 
                            flex: 1.05,
                        }}>
                            {renderHeart()}
                            
                            {renderVivodImage()}
                        </View>
                        
                    </View>
                </View>

                <View style={{flexDirection: 'row', marginTop: 30}}>
                    <Text style={styles.textlight}>Вывод:  </Text>
                    <Text style={styles.textred}>{product.vivod}</Text>
                </View>
                <View style={{marginTop: 5}}>             
                    <Text style={styles.textlight}>Производитель</Text>
                    <Text style={styles.textdark}>{product.proizvoditel}</Text>
                </View>

                {renderDopOpisanie()}

                <View style={{marginTop: 10}}>             
                    <Text style={styles.textlight}>Состав</Text>
                    <Text style={styles.textdark}>{product.sostav}</Text>
                </View>

                {renderNezhelDobavki()}

                
                {renderGdekupit()}
                

                <Text style={styles.textPrivate2}>На упаковке:</Text>
                <View style={{flexDirection: 'row', marginTop: 5, marginBottom:0, width: '90%', marginLeft: 10}}>
                    <View style={{width: '25%', alignItems: 'center'}}>
                        <Text style={styles.textlighttable}>Ккал</Text>
                        <Text style={styles.textdarktable}>{product.kkal}</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center'}}>
                        <Text style={styles.textlighttable}>Белки</Text>
                        <Text style={styles.textdarktable}>{product.belki}</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center'}}>
                        <Text style={styles.textlighttable}>Жиры</Text>
                        <Text style={styles.textdarktable}>{product.zhir}</Text>
                    </View>
                    <View style={{width: '25%', alignItems: 'center'}}>
                        <Text style={styles.textlighttable}>Углеводы</Text>
                        <Text style={styles.textdarktable}>{product.uglevodi}</Text>
                    </View>
                </View>
                
                {renderFact()}

                <View style={{marginTop: 25}}>
                    <FormButtonSmall
                        buttonTitle='Поделиться'
                        onPress={onShare}
                    />
                </View>
                <TouchableOpacity style={{width: '100%', alignItems: 'center', marginTop: -10, marginBottom: 25}} onPress={() => openWpUrl()}>
                    <Text style={styles.textPrivate}>Сообщить об ошибке</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    
   

    
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
    },
    flatlist: {
        padding: 20,
    },
    item: {
        width: Dimensions.get('screen').width, 
        height: Dimensions.get('screen').width * 0.83 * 0.9,
        backgroundColor: '#fff',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 0,
    },
    item2: {
        borderColor: '#000',
        width: '100%',
        height: '100%', 
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemTitle: {
        marginLeft: 3,
        fontFamily: 'roboto-regular', 
        fontSize: 10, 
        color: '#000',
    },
    image: {
        height: '80%',
        resizeMode: 'contain',
        width: '80%', 
        flex: 5, 
    },
    textlight: {
        marginLeft: 30,
        fontFamily: 'roboto-regular', 
        fontSize: 14, 
        fontWeight: 'bold', 
        textAlign: 'left',
        color: '#ACABAB',        
        marginBottom: 5,
    },
    textred: {
        fontFamily: 'roboto-regular', 
        fontSize: 14, 
        fontWeight: 'bold', 
        textAlign: 'left',
        color: 'black',
        marginBottom: 5,
    },
    textdark: {
        marginLeft: 30,
        marginRight: 30,
        fontFamily: 'roboto-regular', 
        fontSize: 14    , 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 0,
    },
    textlighttable: {
        fontFamily: 'roboto-regular', 
        fontSize: 14, 
        fontWeight: 'bold', 
        textAlign: 'left',
        color: '#ACABAB',        
    },
    textdarktable: {
        fontFamily: 'roboto-regular', 
        fontSize: 14    , 
        fontWeight: 'normal', 
        textAlign: 'left', 
        color: '#303030',
    },
    textPrivate: {
        marginTop: 30,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 75,
    },
    textPrivate2: {
        marginLeft: 30,
        marginTop: 20,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },
})