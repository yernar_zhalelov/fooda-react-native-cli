import React, {useState} from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView, Alert, Platform, SafeAreaView, ToastAndroid, Dimensions } from 'react-native'
import { NavigationBar } from '../components/NavigationBar'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import * as ImagePicker from 'expo-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage'
import mime from 'mime'
import Toast from 'react-native-root-toast';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { FormButtonSmall } from '../components/FormButtomSmall'
import { ProductSentModal } from '../modals/ProductSentModal'
import { Root } from '../components/Root'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {url} from './AppConfig'

const plus = require('../assets/images/icoPlus2.png')

let options = {
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

let options2 = {
    title: 'Select Image',
    customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
    ],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export const AddProductScreen = ({navigation}) => {

    const [productSentModall, setProductSentModal] = useState(false)
    const [file, setFile] = useState(null)
    const [file2, setFile2] = useState(null)
    const [file3, setFile3] = useState(null)
    const [comment, setComment] = useState('')

    const renderImage = (status) => {
        if(status == 1) {
            if(file == null) {
                return plus
            } else {
                return {isStatic: true, uri: file.uri}
            }
        } else if(status == 2) {
            if(file2 == null) {
                return plus
            } else {
                return {isStatic: true, uri: file2.uri}
            }
        } else {
            if(file3 == null) {
                return plus
            } else {
                return {isStatic: true, uri: file3.uri}
            }
        }
    } 

    const camera = async (status) => {
        launchCamera(options, (response) => {

            if(response.errorCode) {
      
                Toast.show('Ошибка', {
                    duration: Toast.durations.SHORT, 
                    position: Toast.positions.CENTER
                })
      
            } else if(response.didCancel) {
                console.log("USER CANCELLED")
            } else {

                if(status == 1) {
                    setFile(response.assets[0])
                } else if(status == 2) {
                    setFile2(response.assets[0])
                } else {
                    setFile3(response.assets[0])
                }

            }
      
        })

    }
    
    const library = async (status) => {

        launchImageLibrary(options2, (response) => {
            if(response.errorCode) {
      
                Toast.show('Ошибка', {
                    duration: Toast.durations.SHORT, 
                    position: Toast.positions.CENTER
                })
      
            } else if(response.didCancel) {
              console.log("User cancelled")
            } else {
                if(status == 1) {
                    setFile(response.assets[0])
                } else if(status == 2) {
                    setFile2(response.assets[0])
                } else {
                    setFile3(response.assets[0])
                }
            }
      
        })
    }

    const showAlert = (status) => {
    Alert.alert(
        'Фото',
        'Выберите один из вариантов',
        [
        {
            text: "Камера",
            onPress: () => camera(status)
        }, 
        {
            text: "Библиотека",
            onPress: () => library(status)
        }, 
        {
            text: "Отмена",
            onDismiss: () => console.log("Отмена")
        }
        ], 
        {
        cancelable: true, 
        onDismiss: () => console.log('On dismiss')
        }
    )
    }

    const sendToParse = () => {
        if(file == null || file2 == null || file3 == null) {
            Toast.show('Заполните все данные', {
                duration: Toast.durations.SHORT, 
                position: Toast.positions.CENTER
            })
        } else {

            AsyncStorage.getItem('token').then((token) => {

                var temp = {
                    uri: file.uri, 
                    type: mime.getType(file.uri), 
                    name: file.uri.split('/').pop()
                }
    
                var temp2 = {
                    uri: file2.uri, 
                    type: mime.getType(file2.uri), 
                    name: file2.uri.split('/').pop()
                }
    
                var temp3 = {
                    uri: file3.uri, 
                    type: mime.getType(file3.uri), 
                    name: file3.uri.split('/').pop()
                }
    
                var dataform = new FormData()
                dataform.append('file', temp)
                dataform.append('file2', temp2)
                dataform.append('file3', temp3)

                console.log(temp)
                console.log(temp2)
                console.log(temp3)
                console.log(dataform)

                fetch(url + '/api/send/toParsing?comment=' + comment, {
                    method: 'POST', 
                    headers: {
                        'Authorization': `Bearer ${token}`, 
                        'Content-Type': 'multipart/form-data'
                    },
                    body: dataform
                    
                }).
                then((response) => response.json()).
                then((responseJson) => {
                    
                    if(responseJson.success == true) { 
                        Toast.show('Продукт отправлен', {
                            duration: Toast.durations.SHORT, 
                            position: Toast.positions.CENTER
                        })
                        

                        setFile(null)
                        setFile2(null)
                        setFile3(null)
                        setComment('')


                        navigation.goBack()
                    } else {

                        Toast.show(JSON.stringify(responseJson.error), {
                            duration: Toast.durations.SHORT, 
                            position: Toast.positions.CENTER
                        })

                    }

                }).
                catch((error) => {
                    if(Platform.OS == 'android') {
                        console.log(JSON.stringify(error))
                    } else {
                        Alert.alert(error)
                    }
                })
                
            })

        }
    }

    return <View style={styles.container}>
                <NavigationBar onPress={() => navigation.goBack()} screenTitle='Добавить продукт' />
                <KeyboardAwareScrollView>
                <ProductSentModal 
                    visible={productSentModall} 
                    onClose={()=>setProductSentModal(false)} />  

               
                <Text style={styles.textdark}>Приложите фото продукта*</Text>
                <View style={{flexDirection: "row", width: 322, heigth: 100, marginLeft: (screenWidth - 322) * 0.5}}>
                    <View style={{width: 100, heigth: 100, marginLeft: 5}}>
                        <TouchableOpacity style={styles.touchPlus} onPress={() => showAlert(1)}>
                            <Image style={styles.plusImage} source={renderImage(1)} />
                        </TouchableOpacity>
                        <Text style={styles.itemTitle}>Фото этикетки</Text>
                    </View>
                    <View style={{width: 100, heigth: 100, marginLeft: 5}}>
                        <TouchableOpacity style={styles.touchPlus} onPress={() => showAlert(2)}>
                            <Image style={styles.plusImage} source={renderImage(2)} />
                        </TouchableOpacity>
                        <Text style={styles.itemTitle}>Фото состава продукта</Text>
                    </View>
                    <View style={{width: 100, heigth: 100, marginLeft: 5}}>
                        <TouchableOpacity style={styles.touchPlus} onPress={() => showAlert(3)}>
                            <Image style={styles.plusImage} source={renderImage(3)} />
                        </TouchableOpacity>
                        <Text style={styles.itemTitle}>Фото БЖУ и производитель</Text>
                    </View>
                </View>
                <Text style={styles.textdark2}>Ваш комментарий</Text>
                <View style={styles.inputContainer}>
                    <TextInput 
                        value={comment}
                        style={styles.input}
                        multiline
                        placeholder='Введите комментарий при необходимости'
                        placeholderTextColor='#5E5E5E'
                        autoCorrect={false}
                        keyboardType={Platform.OS === 'android' ? 'visible-password' : 'default'}
                        onChangeText={(text) => setComment(text)}
                        textAlignVertical='top'
                    />
                </View>
                <Text style={styles.textPrivate}>* - обязательные к заполнению поля</Text>
                <View style={styles.sendButton}>
                <FormButtonSmall
                    onPress={()=>sendToParse()}
                    buttonTitle='Отправить'
                />
                </View>
                </KeyboardAwareScrollView>
            </View>
            
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1, 
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    textdark: {
        marginTop: 10,
        marginLeft: 25,
        paddingBottom: 20,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: '700', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },
    itemTitle: {
        textAlign: 'center',
        marginLeft: 3,
        fontFamily: 'roboto-regular', 
        fontSize: 12, 
        color: '#000',
        height: 30
    },
    textdark2: {
        marginTop: 50,
        marginLeft: 25,
        paddingBottom: 20,
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: '700', 
        textAlign: 'left', 
        color: '#303030',
        marginBottom: 5,
    },
    input: {
        padding: 10,
        marginLeft: 5,
        flex: 1,
        fontSize: 14,        
        fontFamily: 'roboto-regular',
        color: '#303030',
        textAlignVertical: 'center',
        ...Platform.select({
            ios: {
                
            }
        })
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 10,
        marginLeft: (screenWidth - 321) * 0.5,  
        width: 321,   
        height: 150,
        borderColor: '#ACABAB',
        borderRadius: 10,
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    textPrivate: {
        marginBottom: 15,
        marginLeft: 25,
        flexDirection: 'row', 
        flexWrap: 'wrap',
        justifyContent: 'center',
        textAlign: 'left', 
        color: '#ACABAB'
    },
    touchPlus: {
        width: 100, 
        height: 100, 
        justifyContent: 'center', 
        alignItems: 'center', 
        flex: 1, 
    }, 
    plusImage: {
        alignSelf: 'center', 
        justifyContent: 'center', 
        height: 50,
        width: 50, 
        resizeMode: 'cover', 
    }, 
    sendButton: {
        marginBottom: 20
    }
})