import React, {useState} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ToastAndroid, Linking, Image, SafeAreaView } from 'react-native'
import { bootstrap } from '../utils/bootstrap'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-root-toast';
import ArrowBack from '../assets/svg/ArrowBack';

import { FormInput } from '../components/FormInput'
import { FormButton } from '../components/FormButton'

import {url} from './AppConfig'
import { ifIphoneX } from 'react-native-iphone-x-helper';

export const RegisterScreen = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    function openUrl() {
        Linking.canOpenURL('https://fooda-app.ru/privacy-policy').then((result) => {
            
            if(result == true) {
                Linking.openURL('https://fooda-app.ru/privacy-policy')
            } else {
                Toast.show('Невозможно открыть сайт', {
                    duration: Toast.durations.SHORT, 
                    position: Toast.positions.CENTER
                })
            }
        }) 
    }

    const registerUser = () => {
        if(email == '' || password == '') {
            ToastAndroid.show('Проверьте введенные данные', ToastAndroid.SHORT)
        } else if(password.length < 8) {
            Toast.show('Пароль должен содержать не менее 8 символов', {
                duration: Toast.durations.LONG, 
                position: Toast.positions.CENTER
            })
        }else {
            fetch(url + `/api/register?email=${email}&password=${password}`, {
                method: 'POST', 
                headers: {},
            }).
            then((response) => response.json()).
            then((responseJson) => {
                console.log('asiodfjoaidsjf == ', responseJson)

                if(responseJson.error == 'this email exist') {
                    Toast.show('Пользователь уже зарегистрирован. Зайдите через свой email и пароль', {
                        duration: Toast.durations.LONG, 
                        position: Toast.positions.CENTER
                    })
                } else if(responseJson.status == 200){
                    Toast.show('Регистрация прошла успешно. Вы можете войти используя свой логин и пароль', {
                        duration: Toast.durations.LONG, 
                        position: Toast.positions.CENTER
                    })

                    navigation.reset({
                        index: 0,
                        routes: [{name: 'Login'}],
                    })

                }

                
            }).
            catch((error) => {
                console.log(JSON.stringify(error))
            })
        }
        
    }

    return <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}> 
    <View style={styles.container}>

        <View style={{
            flexDirection: 'row', 
            marginTop: 45,
            marginLeft: 10,
            width: '45%', 
            justifyContent: 'space-between'
        }}>

            <TouchableOpacity onPress={() => navigation.goBack()} style={{
                justifyContent: 'center', 
                width: 20
            }}>

                {/* <Image style={{alignSelf: 'center'}}  source={require('../assets/images/icoBackArrow.png')} />    */}

                <View style={{
                    alignSelf: 'center', 
                    width: 15,
                    height: 15,
                }}>
                    <ArrowBack/>
                </View>

            </TouchableOpacity>

            <Text style={styles.text}>FOODA</Text>

        </View>


        
        <Text style={styles.text2}>Регистрация аккаунта</Text>
        <FormInput
            placeholderText='E-mail' 
            iconType='mail'
            labelValue={email}
            changeText={(text) => setEmail(text.trim())}
        />
        <FormInput
            placeholderText='Пароль' 
            iconType='password'
            labelValue={password}
            changeText={(text) => setPassword(text.trim())}
        />
        <FormButton 
            buttonTitle='ОФОРМИТЬ ПОДПИСКУ'
            onPress={()=>registerUser()}
        />
        <TouchableOpacity style={{width: '100%', alignItems: 'center', marginTop: 70, marginBottom: 15}} onPress={() => openUrl()}>
            <Text style={styles.navButtonText}>Оформляя подписку, вы принимаете</Text>
            <Text style={styles.navButtonText2}>Политику конфиденциальности</Text>
        </TouchableOpacity>
    </View>
    </SafeAreaView>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
    },
    text: {
        fontFamily: 'roboto-bold', 
        fontSize: 32, 
        fontWeight: 'bold', 
        textAlign: 'left', 
        color: '#85B057',        
    },
    text2: {
        marginTop: 40,
        fontFamily: 'roboto-regular', 
        fontSize: 22, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 35,
    },
    textPrivate: {
        marginBottom: 40,
        flexDirection: 'row', 
        flexWrap: 'wrap',
        justifyContent: 'center',
        textAlign: 'center', 
        color: '#ACABAB'
    },
    navButtonText: {         
        fontSize: 12, 
        fontWeight: 'normal', 
        color: '#848199',
        fontFamily: 'roboto-regular',
    },
    navButtonText2: {         
        fontSize: 12, 
        fontWeight: 'normal', 
        color: '#848199',
        fontFamily: 'roboto-regular',
        textDecorationLine: 'underline',
    },
})