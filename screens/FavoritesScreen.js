import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, ScrollView, FlatList, Dimensions, Image, ToastAndroid } from 'react-native'
import { bootstrap } from '../utils/bootstrap'
import { screenWidth, screenHeight } from '../utils/Dimentions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { NavigationBar } from '../components/NavigationBar'
import { SearchInput } from '../components/SearchInput'
import { ProductItem } from '../components/ProductItem'

import { Root } from '../components/Root'

import {url} from './AppConfig'



const numColumns = 2;

export const FavoritesScreen = ({navigation}) => {
    const [favs, setFavs] = useState('')
    const [findProduct, setFindProduct] = useState('')

    const getData = async () => {
        await AsyncStorage.getItem('favourites').then((favourites) => {
            setFavs(JSON.parse(favourites))
        })
    }

    useEffect(() => {
        getData()

        const unsubscribe = navigation.addListener('focus', () => {
            getData()
        })
        
        return unsubscribe
    }, [])

    const renderFoundItems = () => {
        if(findProduct.trim() == '') {
            return favs
        } else {
            let newData, searchingData;

            newData = favs.filter((item) => {
                searchingData = findProduct.toLowerCase()
                
                return item.nameProduct.toLowerCase().includes(searchingData)
            })
    
            return newData
        }

       
    }

    return <View style={styles.container}>
                <NavigationBar onPress={() => navigation.goBack()} screenTitle='Избранные товары' />
                <SearchInput placeholderText='Поиск продукта в избранном' setText={(item) => setFindProduct(item)}/>
                <FlatList
                    data={renderFoundItems()}
                    style={styles.flatlist}
                    renderItem={({item, index })=><ProductItem item={item} index={index} navigation={navigation} />}
                    numColumns={numColumns}
                />
            </View>
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    flatlist: {
       paddingHorizontal: 10, 
    },
    item: {
        width: '100%', 
        height: '100%',
        backgroundColor: '#fff',
 
        flex: 1,
        margin: 1,
        height: Dimensions.get('screen').width / numColumns, // approximate a square
    },
    item2: {
        borderColor: '#000',
        width: '100%', 
        height: '80%', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },    
})