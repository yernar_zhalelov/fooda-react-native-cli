import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, Image, FlatList, ToastAndroid, Alert } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ifIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper'

import { NavigationBar } from '../components/NavigationBar'
import { SearchInput } from '../components/SearchInput'
import { FormButtonSmall } from '../components/FormButtomSmall'
import { Root } from '../components/Root'
import {url} from './AppConfig'
import { ProductItem } from '../components/ProductItem'

export const SearchScreen = ({route, navigation}) => {
    const [products, setProducts] = useState([])
    const [findProduct, setFindProduct] = useState('')

    useEffect(() => {
        AsyncStorage.getItem('allProducts').then((products) => {
            setProducts(JSON.parse(products))
        })
    }, [])

    const renderFoundItems = () => {
        let newData, searchingData;

        newData = products.filter((item) => {
            searchingData = findProduct.toLowerCase().split(' ')

            let temp = item.nameProduct.toLowerCase().split(' ').join('')

            let result = searchingData.every((e) => temp.includes(e))

            if(result) {
                return item
            }
            
            
        })

        return newData
    }

    const renderBody = () => {
        if(findProduct.trim() == '') {
            return (
                <View>
                    <View style={{alignItems:'center', marginTop: 100}}>
                    <Image source={require('../assets/images/icoSearchBig.png')} />
                    </View>
                    <Text style={styles.text2}>Приложение с разбором продуктов</Text>
                    <Text style={styles.textdark}>Вы можете прислать нам свой продукт для разбора</Text>
                    <View style={{marginTop: 80}}>
                        <FormButtonSmall
                            onPress={()=>navigation.navigate('AddProduct')}
                            buttonTitle='Добавить продукт'
                        />
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.flatlist}>
                    <FlatList
                        data={renderFoundItems()}
                        renderItem={({item, index })=><ProductItem item={item} index={index} navigation={navigation}/>}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        style={{
                            width: '95%', 
                            alignSelf: 'center', 
                            marginBottom: 110
                        }}
                    />
                </View>
            )
        }
    }

    return <View style={styles.container}>
                <NavigationBar onPress={() => navigation.goBack()} screenTitle='Поиск продукта' />
                <SearchInput placeholderText='Поиск продукта' setText={(item) => setFindProduct(item)}/>
                {renderBody()}
            </View>
            
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        ...ifIphoneX({
            paddingTop: getStatusBarHeight()
        })
    },
    text2: {
        fontFamily: 'roboto-regular', 
        fontSize: 16, 
        fontWeight: 'bold', 
        textAlign: 'center', 
        color: '#303030',
        marginTop: 30,
        marginBottom: 5,
    },
    textdark: {
        marginTop: 5,
        marginLeft: 30,
        marginRight: 30,
        fontFamily: 'roboto-regular', 
        fontSize: 14    , 
        fontWeight: 'normal', 
        textAlign: 'center', 
        color: '#303030',
        marginBottom: 5,
        width: '50%', 
        alignSelf: 'center'
    },
    flatlist: {
        alignSelf: 'center',
        width: '95%',
        marginBottom: 50, 
        justifyContent: 'center'
    }, 
})