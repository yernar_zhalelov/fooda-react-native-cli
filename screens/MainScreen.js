import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Button, Modal, TouchableOpacity, Image, ImageBackground, ToastAndroid } from 'react-native'
import { screenWidth, screenHeight } from '../utils/Dimentions'

import AsyncStorage from '@react-native-async-storage/async-storage'

import {url} from './AppConfig'
import SplashScreen from '../assets/svg/SplashScreen';

export const MainScreen = ({navigation}) => {  
    const [api1, setApi1] = useState(false)
    const [api2, setApi2] = useState(false)
    const [api3, setApi3] = useState(false)

    function getData() {
        AsyncStorage.getItem('token').then((token) => {
            if(token != null) {

                fetch(url + `/api/get/all/product`, {
                    method: 'GET', 
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).
                then((response) => response.json()).
                then((responseJson) => {
            
                    AsyncStorage.setItem('allProducts', JSON.stringify(responseJson.data))

                    setApi1(true)
            
                }).
                catch((error) => {
                    ToastAndroid.show(JSON.stringify(error), ToastAndroid.LONG)
                })

                //asdiufhaisudhfiudhf
                
                fetch(url + `/api/show/chosen?token=Bearer ${token}`, {
                    method: 'GET', 
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).
                then((response) => response.json()).
                then((responseJson) => {
                    AsyncStorage.setItem('favourites', JSON.stringify(responseJson.data))

                    setApi2(true)
                }).
                catch((error) => {
                    console.log(JSON.stringify(error))
                })

                //aosdfjoiasjdfioj

                fetch(url + `/api/profile?token=Bearer ${token}`, {
                    method: 'GET', 
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).
                then((response) => response.json()).
                then((responseJson) => {

                    AsyncStorage.setItem('user', JSON.stringify(responseJson.data))

                    setApi3(true)
                }).
                catch((error) => {
                    console.log(JSON.stringify(error))
                })

            } else {

                setTimeout(() => {
                    navigation.replace('Login')
                }, 500);

                
            }
        })
    }

    useEffect(() => {
           
        async function managing() {
            let token = await AsyncStorage.getItem('token')
            
            if(token != null) {
                if(api1 == true && api2 == true && api3 == true) {
                    navigation.replace('Tabs')
                }
            }
        }

        getData()
        managing()
            
    })

    const [modalVisible, setModalVisible] = useState(false);

    const unsubscribe = navigation.addListener('focus', () => {
        setModalVisible(false);
    });

    return (
        <View style={styles.container}>
            <View style={styles.mainbutton}
                underlayColor={"#E5E5E5"}
                onPress={() => {setModalVisible(true)}} >
                <View style={styles.container2}>
                    
                    <View style={{position: 'absolute'}}>
                        <SplashScreen />
                    </View>
                    
                    <View style={styles.fTextHolder}>
                        <Text style={styles.fText}>FOODA</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //backgroundColor: '#fff',
        backgroundColor: '#85B057',
        width: '100%',
        height: '100%',
    },
    container2: {
        backgroundColor: '#85B057',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainbutton: {
        width: '100%', 
        height: '100%',
    },
    button: {
        width: screenWidth/5, 
        height: 72, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    fImage: {
        height: 352, 
        width: 165,
    },
    fTextHolder: {
        flex:1,
        alignItems:'center',
        justifyContent: 'center', 
    },
    fText: {
        fontFamily: "roboto-bold",
        fontSize: 36,
        color: 'white',
        fontWeight: 'bold',
    },
})