import React, { useState, useEffect, useRef, useLayoutEffect} from 'react';
import { View, Text, KeyboardAvoidingView, ToastAndroid, DefaultTheme, Linking, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { RootSiblingParent } from 'react-native-root-siblings';

import NavigatorView from './navigation/NavigatorView';

const MyTheme = {
  ...DefaultTheme,
  colors: {
   
    background: '#F1F1F8'
  },
};


const Stack = createStackNavigator();

function App() {
  const[isReady, setIsReady] = useState(false)
  const navigationRef = useRef(null)

  function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  useEffect(() => {

    Linking.getInitialURL().then((url) => {

      let params = url.split('?product=')[1]
      params = replaceAll(params, '%20', ' ')

      if(params != undefined) {

        AsyncStorage.getItem('id').then((id) => {

          setTimeout(() => {
            if(id != undefined) {
  
              navigationRef.current?.navigate('ProductDetails', {product: params})
    
            } else {
              
              navigationRef.current?.navigate('Login')
              
            }
          }, 1000);
        })

      }

    })

    Linking.addEventListener('url', (event) => {

      let params = event.url.split('?product=')[1]
      params = replaceAll(params, '%20', ' ')
      
      if(params != undefined) {

        AsyncStorage.getItem('id').then((id) => {

          setTimeout(() => {
            if(id != undefined) {
  
              navigationRef.current?.navigate('ProductDetails', {product: params})
    
            } else {
              
              navigationRef.current?.navigate('Login')
              
            }
          }, 1000);
        })

      }
      
    })

  }, [])

  return (
    <RootSiblingParent>
      <NavigationContainer theme={MyTheme} ref={navigationRef}>
      <NavigatorView></NavigatorView>
    </NavigationContainer>
    </RootSiblingParent>
  );
}

export default App;